import { render } from '../../utils/renderer.js';
import { isValidSiteKey } from '../../utils/santizer.js';
import * as errors from '../../utils/errors.js';

const isAsyncRequest = req => req.headers["sec-fetch-dest"] == 'empty';

export const validateSiteKey = (req, res, next ) =>{
    let siteKey = req.params.siteKey;
    if( !isValidSiteKey( siteKey ) ){
        if( isAsyncRequest( req ) ){
            let error = new Error('Bad siteKey');
            error.code = error.status = 400;
            res.status( error.code );

            return render('error', { req, res }, {
                title: `Error - Bad siteKey`,
                error
            });
        }
        return errors.MissingOrInvalidParams( res, 'Bad siteKey');
    }
    res.locals.siteKey = siteKey;
    next();
}