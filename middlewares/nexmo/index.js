import Vonage from '@vonage/server-sdk';
import {isDev} from '../../config/flags.js';
import {getDirname} from '../../utils/helpers.js';
const __dirname  = getDirname(import.meta.url);
//const callerNumber = 12028641844;//EqualWeb

export default new Vonage({
  apiKey: "5211037d",
  apiSecret: process.env.nexmo_secret,
  applicationId: process.env.nexmo_app_id,
  privateKey: `${__dirname}/private.key`
  // credentials : { 
  //   privateKey: './private.key'
  // }
},{
    debug: isDev
});