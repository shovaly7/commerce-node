import vonage from './index.js';


/**
 * TODO: 
 * validate phonenumber format
 * https://developer.nexmo.com/messaging/sms/guides/custom-sender-id#valid 
 * @param {*} targetCellphone 
 * @param {*} code 
 * @returns 
 */
const sendSMS = (targetCellphone, code) =>{
    return new Promise((resolve, reject) =>{
        if( !code ) reject('No code supplied!');

        vonage.message.sendSms(
            process.env.env_name, 
            targetCellphone || "972542503330",
            `Authentication code: ${ code }`, 
            (err, responseData) => {
                if (err) {
                    console.log(err);
                    reject( err );
                } else {
                    if(responseData.messages[0]['status'] === "0") {
                        resolve('SMS sent!');
                    } else {
                        reject(`Message failed with error: ${responseData.messages[0]['error-text']}`);
                    }
                }
            }
        )
    });
}

export default {
    sendSMS,
    getRandomCode
}