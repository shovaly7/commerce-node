export default {
    "en" : {
        voice : "Joey",
        text : code => `
        <speak>
            <s>Hello, This is EqualWeb voice call service.</s>
            <s>Please enter the following code:
                <prosody rate="x-slow">
                    <say-as interpret-as="digits">${code}</say-as>
                </prosody><break strength="medium"/>
            </s>
        </speak>`,
    },
    "he" : {
        voice : "Carmit",
        text : code => 
        `שלום, זוהי מערכת השיחות האוטומטיות של חברת EqualWeb. נא הזינו את הקוד הבא: ${code}`
    }
}