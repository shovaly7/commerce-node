import vonage from './index.js';

const reportError = err =>{
    return new Error( err );
}
/**
 * TODO: 
 * validate phonenumber format
 * https://developer.nexmo.com/messaging/sms/guides/custom-sender-id#valid 
 * @param {string} targetCellphone 
 * @returns {string} request ID
 */
export const sendPinCode = targetCellphone =>{
    return new Promise((resolve, reject) =>{
        vonage.verify.request({
            number: targetCellphone || "+972542503330",
            brand: process.env.env_name,
            code_length : 6
        }, (err, result) => {
            if(err) {
                return reject( err );
            } else {
                if( result.status == '0' ) {
                    return resolve( result.request_id );
                } else {
                    return reject( reportError( result.error_text ) )
                }
            }
        });
    });
}
/**
 * Verify code by its ID
 * @param {*} requestID 
 * @param {*} pin 
 * @returns ? 
 * @TODO: Decide how to handle response from Vonage API..
 * What are the responses code for this request?
 */
export const verifyCode = (requestID, pin) =>{
    return new Promise((resolve, reject) =>{
        vonage.verify.check({
            workflow_id : 1,
            request_id: requestID,
            code : pin
        }, (err, result) => {
            if(err) {
                return reject(err);
            } else {
                if( result?.status == '0' ) { // Success!
                    return resolve('Account verified!');
                } else {
                    // handle the error - e.g. wrong PIN
                    return resolve({
                        error : reportError( result.error_text )
                    });
                }
            }
        });
    })
}

export const cancelVerification = requestID =>{
    return new Promise((resolve, reject) =>{
        vonage.verify.control({
            request_id: requestID,
            cmd: 'cancel'
        }, (err, result) => {
            if (err) {
                return reject(err);
            } else {
                return resolve( result )
            }
        });
    })
}

export default {
    sendPinCode,
    verifyCode,
    cancelVerification
}