import vonage from './index.js';
import nccos from './ncco.js';
import {getRandomInt} from '../../utils/helpers.js';

export const generatePinCode = () => getRandomInt(111111, 999999);

const makeCodeReadable = code =>{
    code = code.toString();
    return code.split('').join('. ');
}

export const sendPinByVoice = ({
    destNumber,
    lang,
    code 
}) => {
    let msgAndVoice = nccos[ lang ] || nccos['en'];
    const ncco = [{
        action: 'talk',
        level : '1',
        loop : 2,
        voiceName: msgAndVoice.voice,
        text : msgAndVoice.text( makeCodeReadable( code ) )
    }];

    return new Promise((resolve, reject) =>{
        if( !code ) reject('No code supplied!');

        vonage.calls.create({
            to: [{
                type: 'phone',
                number : destNumber
            }],
            from: {
                type: 'phone',
                number: '12028641844'
            },
            ncco
        }, (error, response) => {
            if (error) {
                console.error(error);
                return reject({ 
                    type: 'call',
                    error
                });
            }
            if (response) {
                console.log( response );
                return resolve({ 
                    type: 'call',
                    result : 'Phone call started..'
                });
            }
        })
    });
}

export const sendPinBySMS = ({
    destNumber,
    lang,
    code 
}) => {
    return new Promise((resolve, reject) =>{
        if( !code ) reject('No code supplied!');

        vonage.message.sendSms(
            process.env.env_name, 
            destNumber,
            `Authentication code: ${ code }`, 
            (err, responseData) => {
                if (err) {
                    console.log(err);
                    reject({ 
                        type: 'sms',
                        error : err
                    });
                } else {
                    if(responseData.messages[0]['status'] === "0") {
                        resolve({
                            type: 'sms',
                            result : 'SMS sent successfully',
                        });
                    } else {
                        reject({ 
                            type: 'sms',
                            error : `Message failed with error: ${responseData.messages[0]['error-text']}`
                        });
                    }
                }
            }
        )
    });
}

export const sendPinByBoth = ({
    destNumber,
    lang,
    code,
    sms,
    call
}) => {
    return new Promise((resolve, reject) =>{
        if( !code ) reject('No code supplied!');
        let promises = [];
        if( sms ) 
            promises.push(
                sendPinBySMS({
                    destNumber,
                    lang,
                    code 
                })
            )
        if( call )
            promises.push(
                sendPinByVoice({
                    destNumber,
                    lang,
                    code 
                })
            ); 
        Promise.all( promises )
            .then( responses =>{
                let results = {};
                responses.forEach( response =>{
                    let result = response.error? 'error' : 'result';
                    results[ response.type ] = {
                        [result] : response.error || response.result
                    };
                })
                resolve( results );
            })
            .catch( err =>{
                cosole.error( err );
                reject( err );
                debugger;
            });
    });
}