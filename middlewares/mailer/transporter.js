import { createTransport } from 'nodemailer';
const sender = process.env.MAIL_USER;
const bcc = null;//'support@equalweb.com';

const htmlTemplate = content =>{
    return `
    <!doctype html>
    <html ⚡4email>
        <head>
            <meta charset="utf-8">
            ${content.css}
        </head>
        <body>
        ${content.body}
        </body>
    </html>`.trim();
};

/**
 * 
 * @param {*} settings {
 * to : string,
 *  subject : string,
 * content : {
 *  body : string,
 *  css : string
 * }
 * }
 * @return Promise
 */
export const sendEmail = ({
    to = '',
    subject = '',
    content = {}
}) =>{
    content = {
        body : content.body || content || '',
        css : content.css || '' 
    }
    return new Promise((resolve, reject) =>{
        try {
            const transporter = createTransport({
                host: process.env.MAIL_HOST,
                port: 587,
                secure: true,
                auth: {
                    user: process.env.MAIL_USER,
                    pass: process.env.MAIL_PASS
                }
            });
    
            transporter.sendMail({
                from: sender,
                to,
                bcc: bcc,
                replyTo: sender,
                subject,
                html: htmlTemplate( content )
            })
                .then( result =>{
                    resolve( result );
                })
                .catch( err =>{
                    reject( err );
                })
        } catch ( err ){ 
            reject( err );
        }
    });
}