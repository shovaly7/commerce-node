import axios from 'axios';
// import querystring from 'querystring';
import qs from 'qs';
import {isDev} from '../../config/flags.js';

const monitorRoot =  `https://${process.env.monitor_server_domain}`;
const adminRoot =  `${monitorRoot}/admin`;
const monitorKey = process.env.admin_monitor_key;
const adminMonitorKey = process.env.monitor_key;

const allowedMethods = {
    GET : 'get',
    POST : 'post',
    DELETE : 'delete',
    PUT : 'put'
}

const defaults = {
    path : '/',
    method : 'GET',
    payload : null,
    params : null,
    admin : false,
    timeout : 5000,
    caller : ''
};

export const apiCall = (...args) =>{
    let firstArg = args[0];
    let otherSettings = args[1];

    let settings = defaults;
    if( typeof firstArg == 'string'){
        settings.path = firstArg;
        if( typeof otherSettings =='object'){ 
            settings = { ...defaults, ...otherSettings};
        }
    } else if( typeof firstArg =='object'){
        settings = {...defaults, ...args};
    }
    return new Promise((resolve, reject) =>{
        const { path, admin, method, payload, params, timeout, caller} = settings;
        let _method = allowedMethods[ method ];
        if( !_method ){
            reject( errorWrapper({
                code : 405,
                error : 'Method Not Allowed'
            }) );
            return;
        }
        axios({
            url : `${ admin? adminRoot : monitorRoot }/${ path }`,
            method: _method,
            // `transformResponse` allows changes to the response data to be made before
            // it is passed to then/catch
            transformResponse: [function (data) {
              try{
                  return JSON.parse( data );
              }
              catch( err ){
                  return data;
              }
            }],
            headers: {
                'x-monitorKey' : admin? adminMonitorKey : monitorKey,
                'x-request-from-client' : caller
                //'content-type': 'application/x-www-form-urlencoded'
            },         
            params,
            paramsSerializer: params => qs.stringify( params ),
            data: payload,
            timeout,
            validateStatus: function (status) {
              return status >= 200 && status < 300; // default
            },                    
        })
            .then( response =>{
                resolve({
                    code: response.status,
                    status : response.statusText,
                    data : response.data,
                });
            })
            .catch( err =>{
                let res = err.response;
                let resType = res.headers['content-type'];
                res.status = res.status == 502? 503 : res.status;

                let error = res.status == 503? 
                    'Monitor API unavailable..' : 
                    res.data.error || 'Unexpected error';
                let resError = {
                    code: res.status,
                    status : res.statusText,
                    error
                }
                if( isDev ) 
                    resError.stack = err.stack;
                console.error( resError );
                reject( resError );
            })
    })

}