import {apiCall} from './index.js';
import {isPositiveInteger, isValidSiteKey} from '../../utils/santizer.js';
import { isDevTestMode } from '../../config/flags.js';
import {getDummyData} from '../../utils/dev-utils.js';

const validateSiteKey = siteKey =>{
    if( !isValidSiteKey( siteKey ) ){
        throw new Error( 'Malformed Site key!');
    }
}

const defaults = {
    payload : null,
    params : null,
    admin : true,
    timeout : 15 * 1000
};

export default class AdminMonitorAPI {
    constructor( caller ){
        this.requestFrom = caller;
    }
    #adminCall(path, settings){
        settings = { ...defaults, ...settings};
        return apiCall( path, settings );
    }
    /**
     * Get all schedules with agregators of "From" & "To"
     * @returns {Promise<Array>} schedules
    */
    getAllSchedules = ( from, to ) =>{
        if( isDevTestMode ){
            return getDummyData('all-schedules')
        }
        return this.#adminCall(`schedule/`, {
            caller : this.requestFrom,
            params: { 
                from : isPositiveInteger( from )? from : '',
                to : isPositiveInteger( to )? to : '',
            }
        })
            .then(schedules =>{
                return schedules.data;
            })
            .catch( response =>{
                let error = new Error( response.error );
                error.code = response.code;
                throw error;
            })
    }
    /**
     * Get last 3 scans of site..
     * @returns {Promise<Array>} scans
    */
    getScansOfSite = siteKey =>{
        validateSiteKey( siteKey  );

        if( isDevTestMode ){
            return getDummyData('demo-scans')
        }
        return this.#adminCall(`scans/`, {
            caller : this.requestFrom,
            params: { 
                siteKey
            }
        })
            .then(schedules =>{
                return schedules.data;
            })
            .catch( response =>{
                let error = new Error( response.error );
                error.code = response.code;
                throw error;
            })
    }
}