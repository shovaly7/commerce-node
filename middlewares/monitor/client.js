import {apiCall} from './index.js';
import {isPositiveInteger, isValidSiteKey} from '../../utils/santizer.js';

const NoUserIDprovided = () =>{};


export default class MonitorAPI {
    constructor({ userID, caller }){
        this.requestFrom = caller;
        this.userID = isPositiveInteger(userID)? userID : null;
    }
    /**
     * Get all User schedules by its local ID
     * @returns {Promise<Array>} schedules
    */
    getAllSchedules(){
        if( !this.userID ){
            console.error('No User ID provided.');
            return [];
        }
        apiCall(`schedules/${ this.userID }`)
            .then(schedules =>{
                debugger;
            })
            .catch( response =>{
                debugger;
            })
    } 
    getSchedule(siteKey){
        apiCall(`schedule/${ siteKey }`)
            .then(schedules =>{
                debugger;
            })
            .catch( response =>{
                debugger;
            })
    }
    getScansDates(siteKey){
        return apiCall(`scans/dates/`,{
            params: { siteKey }
        })
        .then(dates =>{
            return dates.data;
        })
        .catch( response =>{
            let error = new Error( response.error );
            error.code = response.code;
            throw error;
        })
    }
    getSiteLastScanID(siteKey){
        return this.getScansDates( siteKey )
            .then( dates =>{
                return dates && dates.pop() || null;
            })
            .catch( response =>{
                let error = new Error( response.error );
                error.code = response.code;
                throw error;
            })
    }
    getAllScans( siteKey ){
        apiCall(`scans/`,{
            params: { siteKey }
        })
            .then(schedules =>{
                debugger;
            })
            .catch( response =>{
                debugger;
            })
    }
    getReport( scanRef ){
        apiCall(`report/${scanRef}`,{
            params: { userID : this.userID }
        })
            .then(schedules =>{
                debugger;
            })
            .catch( response =>{
                debugger;
            })
    }
    getSiteImages( siteKey ){
        apiCall(`images/${siteKey}`)
            .then(schedules =>{
                debugger;
            })
            .catch( response =>{
                debugger;
            })
    }
}