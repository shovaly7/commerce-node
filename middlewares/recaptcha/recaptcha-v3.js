import axios from 'axios';
import querystring from 'querystring';
import {isDev} from '../../config/flags.js';

export const checkRecaptcha = (req, res, next) => {
    if( isDev ) {
        return next();    
    }
    const token = req.body.token;
    if( !token ) 
        return res.status(400).json({ error: "Missing token" });
        // return console.error('No token provided.');

    axios.post(`https://www.google.com/recaptcha/api/siteverify`, querystring.stringify({
        secret : process.env.google_recaptcha_v3_secret,
        response : token
    }))
        .then(google_response => {
            const {data} = google_response;
            if( data.success ){
                console.log('ReCAPTCHA score:', data.score );
                if( data.score < 0.6 ){
                    return res.status(403).json({ error : 'Low scrore..Try again..'});
                }
                console.log('ReCAPTCHA passed..');
                return next();
            } else{
                let errors = data['error-codes'];
                let error = errors.join(' ');
                console.error( 'Error:', error );
                return res.status(400).json({ error });
            }
        })
        .catch(error => {
            debugger;
            return res.status(500).json({ error });
        });
};
