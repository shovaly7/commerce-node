import NodeCache from 'node-cache';
const passTokens = new NodeCache({
    stdTTL : 60 * 5
});
 
const getDataByKey = key =>{
    let val = passTokens.get( key );
    debugger
    // let data = Object.entries( passTokens )
    //             .find( record => record[1].email == key);
    // return data && data[1] || null;
}
const getDataByToken = token =>{
    return passTokens[token];
}
const saveToken = (key, value ) =>{
    passTokens.set(key, value, Date.now() + 3600000 )
    //for 1 hour
}
const deleteToken = token =>{
    let data = passTokens[token];
    if( data ){
        delete passTokens[token];
    }
}

export default {
    getDataByKey,
    getDataByToken,
    saveToken,
    deleteToken
}