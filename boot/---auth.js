import passport from 'passport';
import Strategy from 'passport-local';
import md5Hash from '../utils/md5-hash.js';
import {logIn} from '../models/user.js';
import {getRole} from '../utils/user-role.js';


export default () =>{

  // Configure the local strategy for use by Passport.
  //
  // The local strategy requires a `verify` function which receives the credentials
  // (`username` and `password`) submitted by the user.  The function must verify
  // that the password is correct and then invoke `cb` with a user object, which
  // will be set at `req.user` in route handlers after authentication.
  passport.use( new Strategy({
    usernameField: 'email',
    passwordField: 'password',
  }, (username, password, cb) =>{
    logIn( username )
        .then( user =>{
            if( !user ){
              //No such user, in DB..
                return cb(null, false, { message: 'Incorrect username or password.' });
            }
            if( user.isLocked ){
              //User is locked..
              return cb(null, false, { message:  'Some of your credentials are incorrect.\n Please contact our support team.' });     
            }
   
            //Check password..
            if( user.password !== md5Hash( password ) ){
              return cb(null, false, { message: 'Incorrect username or password.' });
            }

            return cb(null, {
                id: user.userID.toString(),
                name: user.userName,
                email: user.userEmail,
                phone : user.userPhone,
                passExpiration : user.passExpiration,
                activated : user.activated == 'activated',
                hasTwoFactorAuth : user.hasTwoFactorAuth == 'true',
                loginHash : user.loginHash || null,
                paymentCurrency : user.paymentCurrency || 'USD',
                paymentType : user.paymentType,
                role : getRole( user )
            });
        })
        .catch( err =>{
            return cb(err);
        })
  }));


  // Configure Passport authenticated session persistence.
  //
  // In order to restore authentication state across HTTP requests, Passport needs
  // to serialize users into and deserialize users out of the session.  The
  // typical implementation of this is as simple as supplying the user ID when
  // serializing, and querying the user record by ID from the database when
  // deserializing.
  passport.serializeUser(function(user, cb) {
    process.nextTick(function() {
      cb(null, { 
        id : user.id,
        phone : user.phone,
        name : user.name, 
        email : user.email,
        role : user.role,
        doubleAuthenticated : user.doubleAuthenticated,
        passExpired : user.passExpired,
        activated : user.activated 
      });
    });
  });

  passport.deserializeUser(function(user, cb) {
    process.nextTick(function() {
      return cb(null, user);
    });
  });

};