import { getAllPlans } from '../../models/plan/index.js';
import { isPositiveInteger } from '../../utils/santizer.js';
import logger from '../../boot/logger.js';

class AllPlans{
    constructor(){
        this.cache = {};
        this.widgetPlans = {};
        this.monitorPlans = {};
        this.creditPlans = {};
        this.managedPlans = {}; 
        this.initData();
    }
    initData(){
        getAllPlans()
            .then( plans =>{
                plans.forEach( plan =>{
                    //Put every plan in cache..
                    this.cache[ plan.ID ] = plan;
                    //then sort them by type.. 
                    switch( plan.type ){
                        case 1:
                            if( plan.monthlyPrice == null || plan.monthlyPrice > 0)
                                this.widgetPlans[ plan.ID ] = plan;
                            break;
                        case 2:
                            this.monitorPlans[ plan.ID ] = plan;
                            break;
                        case 3:
                            this.creditPlans[ plan.ID ] = plan;
                            break;
                        case 4:
                            this.managedPlans[ plan.ID ] = plan;
                            break;
                    }
                });
                logger.debug('[[ All plans loaded to cache.. ]]');

            })
            .catch( error =>{
                logger.error( 'An error occurred while loading all plans to cache..', error );
            })
    }
    getPlan( id ){
        if( !isPositiveInteger( id ) ) 
            return false;
        return this.cache[ id ] || null;
    }
    get WidgetPlans(){
        return this.widgetPlans;
    }
    get MonitorPlans(){
        return this.monitorPlans;
    }
    get ManagedPlans(){
        return this.managedPlans;
    }
    get CreditPlans(){
        return this.creditPlans;
    }
}

//Singleton initialization..
export default new AllPlans();