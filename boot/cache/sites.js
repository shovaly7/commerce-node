import NodeCache from 'node-cache';
import { getAllSites } from '../../models/admin/site.js';
import { isValidSiteKey } from '../../utils/santizer.js';
import logger from '../../boot/logger.js';

class AllSites{
    constructor(){
        this.cache = new NodeCache();
        this.initData();
    }
    initData(){
        getAllSites()
            .then( data =>{
                this.cache.mset( 
                    data.map( site => {
                        return {
                            key : site.SiteKey, 
                            val : site
                        } 
                    })
                );
                logger.debug('[[ All sites loaded to cache.. ]]');
            })
            .catch( error =>{
                logger.error( 'An error occurred while loading all sites to cache..', error );
            })
    }
    isSiteExists( siteKey ){
        if( !isValidSiteKey( siteKey ) ) 
            return false;
       return this.cache.has( siteKey ); 
    }
    getSite( siteKey ){
        if( !isValidSiteKey( siteKey ) ) 
            return false;
        return this.cache.get( siteKey );
    }
    getMultipleSites( listOfKeys ){
        if( !Array.isArray( listOfKeys ) ){
            logger.error('Non-array data provided.. try again.');
            return false;
        }
        return this.cache.mget( listOfKeys );
    }
    getAllSiteKeys(){
        return this.cache.keys();
    }
    saveSite( siteKey, data ){
        if( !isValidSiteKey( siteKey ) ) 
            return false;
        return this.cache.set( key, data );
    }
    saveMultipleSites( listOfKeys ){
        if( !Array.isArray( listOfKeys ) ){
            logger.error('Non-array data provided.. try again.');
            return false;
        }
        return this.cache.mset( listOfKeys );
    }
    removeSite( siteKey ){
        return this.cache.del( key );
    }
    removeMultipleSites( listOfKeys ){
        if( !Array.isArray( listOfKeys ) ){
            logger.error('Non-array data provided.. try again.');
            return 0;
        }
        return this.cache.del( listOfKeys );
    }
}

//Singleton initialization..
export default new AllSites();