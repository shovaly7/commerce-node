import sql from 'mssql';
import sqlConfig from '../config/db.js';
import {validateNonEmpty} from '../utils/helpers.js';
import logger from './logger.js';

export default validateNonEmpty;

export const dbErrorHandler = err =>{
    logger.error('DB error:', err);
    throw new Error( err.message );
}

export const dbRequest = async query =>{
    return new Promise((resolve, reject) => {
        new sql.ConnectionPool( sqlConfig )
            .connect()
            .then( pool => pool.request().query( query ) )
            .then(result => {
                resolve( result );
            })
            .catch(err => {
                reject( err );
            })
            .finally( () =>{
                sql.close();
            })
    });
}

logger.info('|| DB set.. ||');