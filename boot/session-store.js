import session from 'express-session';
import initStore from 'session-file-store';
import sessionConfig from '../config/session.js';
import logger from './logger.js';

const FileStore = initStore( session );
const fileStoreOptions = {
    secret : process.env.session_store_secret,
    logFn : logger.debug
};

sessionConfig.store = new FileStore( fileStoreOptions );

logger.info('|| Session store set.. ||');
export default sessionConfig;