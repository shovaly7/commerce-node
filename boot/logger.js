const isDev = process.env.node_env == 'development';
import colors from 'colors/safe.js';

colors.setTheme({
    silly: 'rainbow',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red'
});

const printer = (type, ...inputs) =>{
    let color = colors[ type ] || colors.info;
    console.log( color( inputs.join(' ') ) );
}
const writeToLogFile = (type, input) =>{
    //Should write the log to log file in production..
}
class Logger{
    constructor(){
        this.info('|| Logger is ready.. ||');
    }
    debug( ...logs ){
        if( isDev ){
            printer('debug', ...logs );
        }
    }
    info( ...logs ){
        if( isDev ){
            printer('info', ...logs );
        }
        else writeToLogFile('info', ...logs );
    }
    warn( ...logs ){
        if( isDev )
            printer('warn', ...logs );
    }
    error( ...logs ){
        if( isDev )
            printer('error', ...logs );
        else writeToLogFile('error', ...logs );
    }
    fatal(){
        // if( isDev )
        //     console.log( colors.error( ...logs ) );
        // else writeToLogFile('error', ...logs );
    }
    trace( label ) {
        if( isDev )
            console.trace( colors.prompt( label ) );
    }
    /**
     * Just print as table for now..
     * @param {*} data 
     */
    table( ...data ){
        if( isDev ){
            console.table( ...data );
        }
    }
    flush(){
        if( isDev )
            console.clear();
    }
}

//Singleton initialization..
export default new Logger();