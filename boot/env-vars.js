import logger from './logger.js';
import dotenv from 'dotenv';
const result = dotenv.config();

if (result.error) {
  logger.error('ENV vars load error:', result.error);
  throw result.error;
}

logger.info('|| ENV vars set.. ||');
export default result.parsed;