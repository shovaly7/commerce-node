const createElement = ( tag, {
    id = null,
    className = null,
    content = null,
})=>{
    let elem = document.createElement( tag );
    if( id ) elem.id = id;
    if( className ) elem.className = className;
    if( content ) elem.innerHTML = content;
    return elem;
}

const defaults = {
  expandedClassName : 'gridjs-tr-child-row-expanded'
}
export class GridChildRow{
  constructor( contextBtn, settings ){
    this.settings = {...settings, ...defaults};
    this.trigger = contextBtn;
    this.relatedRow = this.trigger.closest('tr');
    this.table = this.relatedRow.closest('table');
    if( contextBtn.dataset.childRow ){
        return this.table.querySelector( `#${ contextBtn.dataset.childRow }` )._instance;
    }
    this.childRow = null;
  }
  setTriggerState( expanded ){
      let icon = this.trigger.querySelector('i');
      if( expanded ){
          this.trigger.ariaExpanded = 'true';
          icon.classList.add('fa-chevron-circle-up');
          icon.classList.remove('fa-chevron-circle-down');
        } else{
          this.trigger.ariaExpanded = 'false';
          icon.classList.remove('fa-chevron-circle-up');
          icon.classList.add('fa-chevron-circle-down');
      }
      this.hideTriggerTooltip();
  }
  close(){
    delete this.trigger.dataset.childRow;
    this.childRow.remove();
    this.relatedRow.classList.remove( this.settings.expandedClassName );
    this.table.classList.remove( this.settings.expandedClassName );
    this.setTriggerState( false );
  }
  hideTriggerTooltip(){
    bootstrap?.Tooltip.getInstance( this.trigger ).hide();
  }
  render( content ) {
    if(  this.table.contains( this.childRow ) ){
        //already rendered in DOM..
        this.close();
        return;
    }

    this.childRow = createElement('tr', {
        id : 'gridjs-tr-child-row-' + Date.now(),
        className : `gridjs-tr-child-row ${ this.settings.expandedClassName }`
    });

    this.relatedRow.classList.add( this.settings.expandedClassName );
    this.table.classList.add( this.settings.expandedClassName );

    this.childRow._instance = this;
    this.childRow.insertCell(0);
    this.childRowCell = this.childRow.cells[0];
    this.childRowCell.colSpan = this.relatedRow.cells.length;
    this.childRowCell.style.padding = '15px';
    
    let closeBtn = createElement('button', {
        className : 'close gridjs-tr-child-row-close-btn',
        content : `
        <i class="far fa-times"></i>
        <span class="sr-only">Close Row</span>`
    });
    
    closeBtn.addEventListener('click', function (e) {
         this.close();
    }.bind( this ));

    let inContainer = createElement('div', {
        className : 'gridjs-tr-child-row-inner-container'
    });
    let container = createElement('div', {
        className : 'gridjs-tr-child-row-container'
    });
    container.style.position = 'relative';
    
    container.appendChild( closeBtn );
    container.appendChild( inContainer );
    this.childRowCell.appendChild( container );
    
    this.trigger.dataset.childRow = this.childRow.id;
    this.relatedRow.insertAdjacentElement('afterend', this.childRow);
    this.setTriggerState( true );
    if( content ){
        if( content instanceof HTMLElement ){
          inContainer.appendChild( content );
        } else inContainer.innerHTML = content;
    }
  }
}