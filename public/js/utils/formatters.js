export const formatAsDate = dateStr =>{
	try {
		return new Date( dateStr || '' ).toLocaleDateString('en-GB');
	} catch (error) {
		return error.message;
	}
}
export const formatAsDateTime = dateStr =>{
	try {
		return new Date( dateStr || '' ).toLocaleString('en-GB');
	} catch (error) {
		return error.message;
	}
}

export const formatNumberWithCommas = (() => {
	let formater = new Intl.NumberFormat('en-US');
	return number =>{
		let commasFormat = formater.format( parseFloat( number ) );
		return commasFormat == 'NaN'? NaN : commasFormat;
	}
})();

export const removeCommasFromNumber = strNumber => {
	return strNumber && +strNumber.replace(/,/g,'') || '';
}


export const formatDuration = durationObj =>{
	if( !d || typeof d !== 'object' ) return '';
	const getTwoDigits = num =>{
		return num < 10? `0${num}`  : num;
	}
	let days = d.days? `${d.days} day${checkPlural( d.days )} & ` : '';
	let hms  = `${getTwoDigits( d.hours )}:${getTwoDigits( d.minutes )}:${getTwoDigits( d.seconds) }`;
	return days + hms;
}