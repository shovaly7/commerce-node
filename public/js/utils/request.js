/**
 * 
 * @param {*} param0 
 * @returns 
 */
export const GET = ( url, params ) =>{
    return new Promise((resolve, reject) =>{
        let query = '';
        if( params ){
            if( typeof params == 'object'){
                params = new URLSearchParams( params ).toString();
            } else{
                params = params.replace(/^[?]/g,'');
            }
            query = '?' + params;
        } 
        fetch( url + query, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .then( async response => {
                try {
                    let json = await response.json();
                    return response.ok? 
                            resolve( json ) :
                            reject( json );
                } catch( err ){
                    return reject({
                        code : response.status,
                        error: response.statusText,
                    });
                }
            })
    })
}

export const preparePayload = (htmlForm, extraParams) => {
    let formData = new FormData(htmlForm);
    let payload = {};
    for( const key in extraParams){
        payload[key] = extraParams[key];
    }
    formData.forEach( (v, k) =>{
        payload[k] = v;
    });
    
    return JSON.stringify( payload );
}

export const POST = (url, {
    payload,
    form,
    redirectaionTimeout = 2000
} = {}) =>{
    return new Promise((resolve, reject) =>{
        if( form instanceof HTMLFormElement ){
            payload = preparePayload( form, payload );
        } 
        else payload = JSON.stringify( payload );
        fetch( url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            body: payload
        })
            .then(async response => {
                if( response.redirected ){
                    if( new URL( response.url ).host == location.host ){
                        setTimeout( () => {
                            location.replace( response.url );
                        }, redirectaionTimeout || 0 );

                        return resolve({'result' : 'Redirecting..'});
                    } else{
                        return reject({
                            error: 'CORS redirect not allowed..'
                        });
                    }
                }
                let json = await response.json();
                if( response.ok )
                    return resolve( json );
                return reject( json );
            })
    })
}	