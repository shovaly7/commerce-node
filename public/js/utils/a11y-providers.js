export const getA11yProvider = name =>{
	switch( name && name.toLowerCase() ){
		case 'equalweb':
			return {
				name : 'EqualWeb',
				url : 'https://equalweb.com/'
			};
		case 'acsb':
			return {
				name : 'AccessiBe',
				url : 'https://accessibe.com/'
			};
		case 'user1st':
			return {
				name : 'User1st',
				url : 'https://www.user1st.com/'
			};
		case 'audioeye':
			return {
				name : 'AudioEye',
				url : 'https://audioeye.com/'
			};
		case 'mksense': 
			return {
				name : 'Make Sense',
				url : 'https://mk-sense.com/'
			};
		case 'adally': 
			return {
				name : 'Adally',
				url : 'https://adally.com/'
			};
		case 'reciteme': 
			return {
				name : 'Recite Me',
				url : 'https://reciteme.com/'
			};
		case 'enable': 
			return {
				name : 'Enable',
				url : 'https://enable.co.il/'
			};
		case 'userway': 
			return {
				name : 'UserWay',
				url : 'https://userway.org/'
			};
		case 'usablenet': 
			return {
				name : 'UsableNet',
				url : 'https://usablenet.com/'
			};
		case 'monsido': 
			return {
				name : 'monsido',
				url : 'https://monsido.com//'
			};
		case 'maxaccess': 
			return {
				name : 'maxaccess',
				url : 'https://maxaccess.io/'
			};
		default:
			return false;
	}
}