export const cssVar = ((name, value) =>{
	const _root = document.querySelector(':root');
	return (name, value)=>{
		if( !value ){
			return getComputedStyle(_root)?.getPropertyValue( name )?.trim();
		} else{
			//set..
			_root.style.setProperty(name, value);
		}
	}
})();

export const ColorConvert = ( colorStr, opacity )=>{
	colorStr   = colorStr.trim();
	if( /^#[0-9A-F]{6}$/i.test(colorStr) ){
		//HEX --> RGBA
		var hex = colorStr;
		return 'rgba(' + (hex = hex.replace('#', ''))
				.match(new RegExp('(.{' + hex.length/3 + '})', 'g'))
				.map(function(l) {
					return parseInt(hex.length%2 ? l+l : l, 16) 
				}).concat(opacity||1).join(',') + ')';
		//credits :  https://stackoverflow.com/users/1221082/denis
	} else{
		//RGBA --> HEX
		var rgb = colorStr;
		rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		function hex(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		//credits : https://stackoverflow.com/users/424498/erick-petrucelli
	}
}

export const ParseJSON = jsonStr =>{
    try{
        return JSON.parse( jsonStr );
    } catch( err ){
        console.error( 'Parse error:', err.message );
        return false;
    }
}

export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

export const getRecaptchaToken = () =>{
    return new Promise( (res, rej) =>{
        if( !( 'grecaptcha' in window ) ) return res( null );
        grecaptcha.ready(function() {
            grecaptcha.execute('6LfkdVYcAAAAAEZMxfHRs3V45j6rlem1rUvpYeKh', {
                    action: 'submit'
            }).then(function(token) {
                res( token );
            })
            .catch( err =>{
                console.error( err.message );
                rej(null);
            })
        });
    })
}