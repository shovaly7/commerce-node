import { Grid, PluginPosition  } from "/gridjs/dist/gridjs.mjs";
import { columns } from "./columns.js";
import { GridChildRow } from "/js/utils/gridjs-child-row.js";
import { MonitorScans } from "./monitor-scans.js";
import { GET, POST } from "/js/utils/request.js";

( ()=>{
	const tableWrapper = document.getElementById("all-sites-wrapper");
	const schedules = {};
	const grid = new Grid({
		sort: true,
		autoWidth : false,
		columns,
		server: {
			method: 'POST',
			url: '/monitor/all-scans/data',
			then: data => data,
			total: data => {
				return data.length;
				//return 1000
			}
		},
		pagination: {
			enabled: true,
			limit: 50,
			summary: true,
			server: {
				url: (prev, page, limit) => {
					return `${prev}${prev.includes('?')? '&' : '?'}limit=${limit}&offset=${page * limit}`
				}
			}
		},
		search: {
			ignoreHiddenColumns : false,
			selector: (cell, rowIndex, cellIndex) => {
				if( typeof cell === 'object' ){
					//debugger;
					//TODO: should handle those cases somehow.. :|
					return cell.props.content;
				} 
				return cell;
			}
		},
		style: {
			table: { 
				'white-space': 'nowrap',
				'width' : '100%'
			},
			td : {
				'padding': '10px 5px'
			},
			th :{
				'padding': '10px 5px'
			}
		}
	}).render( tableWrapper );

	const getScans = key =>{
		return new Promise((resolve, reject) =>{
			if( schedules[ key ] ){
				return resolve( schedules[ key ] );
			}
			POST(`/monitor/all-scans/data/${ key }`)
				.then( scans =>{
					schedules[ key ] = scans;
					resolve( scans );
				})
				.catch( response =>{
					console.error( response.error || response.message || response );
					reject(false);
				})
		})
	}

	const initTTs = () =>{
		tableWrapper.querySelectorAll('[data-bs-toggle="tooltip"]').forEach( elm =>{
			var tooltip = new bootstrap.Tooltip(elm, {
				boundary: document.body
			})
		})
	}

	grid
		.on('ready', ( e ) => {
			initTTs();
		})
		.on('cellClick', async ( e ) => {
			let btn = e.target.closest('button');
			if( btn ){
				let data = btn.dataset;
				switch( data.method ){
					case 'view':
						let scans = await getScans(  data.key );
						if( scans ){
							let tr = new GridChildRow( btn );
							if( btn.ariaExpanded == 'true' ){
								tr.close();
								return;
							}
							let ms = new MonitorScans( data.domain, scans );
							tr.render( ms.render() );
						}
						break;
				}
			}
		});
})();