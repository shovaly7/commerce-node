import { html } from "/gridjs/dist/gridjs.mjs";
import { 
    formatAsDate, 
    formatNumberWithCommas,
    removeCommasFromNumber
} from "/js/utils/formatters.js";
const defaultFavicon = 'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABs0lEQVR4AWL4//8/RRjO8Iucx+noO0O2qmlbUEnt5r3Juas+hsQD6KaG7dqCKPgx72Pe9GIY27btZBrbtm3btm0nO12D7tVXe63jqtqqU/iDw9K58sEruKkngH0DBljOE+T/qqx/Ln718RZOFasxyd3XRbWzlFMxRbgOTx9QWFzHtZlD+aqLb108sOAIAai6+NbHW7lUHaZkDFJt+wp1DG7R1d0b7Z88EOL08oXwjokcOvvUxYMjBFCamWP5KjKBjKOpZx2HEPj+Ieod26U+dpg6lK2CIwTQH0oECGT5eHj+IgSueJ5fPaPg6PZrz6DGHiGAISE7QPrIvIKVrSvCe2DNHSsehIDatOBna/+OEOgTQE6WAy1AAFiVcf6PhgCGxEvlA9QngLlAQCkLsNWhBZIDz/zg4ggmjHfYxoPGEMPZECW+zjwmFk6Ih194y7VHYGOPvEYlTAJlQwI4MEhgTOzZGiNalRpGgsOYFw5lEfTKybgfBtmuTNdI3MrOTAQmYf/DNcAwDeycVjROgZFt18gMso6V5Z8JpcEk2LPKpOAH0/4bKMCAYnuqm7cHOGHJTBRhAEJN9d/t5zCxAAAAAElFTkSuQmCC';

const datePassed = date =>{
	const now = new Date();
	return (()=>{
		return new Date( date ) < now;
	})();
}

export const columns = [
    {
        name: 'ID',
        data : row => row.site.ID,
        hidden: true,
    }, 
    {
        name: 'Actions',
        data : ( row ) => {
         return html(`
        <div class="more-options-wrapper">
            <div tabindex="0" class="more-options-trigger" 
                id="more-options-${row.site.ID}" aria-haspopup="true" aria-expanded="false">
                <icon></icon>
                <span class="sr-only">More Options</span>
            </div>
            <div class="more-options-container">
                <a 
                    href="/monitor/siteMap/${row.siteKey}" 
                    class="sitemap-link"
                    data-bs-toggle="tooltip" 
                    title="Sitemap and Scan" 
                >
                    <icon></icon>
                    <span class="sr-only">Sitemap and Scan</span>
                </a>
                ${row.lastScan == null? '' : `
                <a 
                    href="/monitor/report/${row.siteKey}"
                    class="view-report-link"
                    data-bs-toggle="tooltip" 
                    title="View Last Report" 
                >
                    <icon></icon>
                    <span class="sr-only">View Last Report</span>
                </a>`}
                <a 
                    href="/monitor/settings/${row.siteKey}" 
                    class="edit-settings-link"
                    data-bs-toggle="tooltip" 
                    title="Advanced Settings" 
                >
                    <icon></icon>
                    <span class="sr-only">Advanced Settings</span>
                </a>
            </div>
        </div>`);
        /**
         *                 <a 
                    data-bs-toggle="tooltip" 
                    title="Delete Site Schedule" 
                    class="delete-schedule-btn"
                    data-method="delete-schedule"
                    data-key="${row.siteKey}"
                >
                    <icon></icon>
                    <strong>Should be admin only!!!</strong>
                    <span class="sr-only">Delete Site Schedule</span>
                </a>
         */
        },
        sort : false
    },
    {
        name: 'Domain',
        data : row => {
            let domain = row.site.Domain || '';
            return html(`
            <a class="site-domain text-truncate" target="_blank" href="http://${domain}">
                <img class="img-thumbnail" alt="${domain}" 
                    src="${
                    row.favicon? 
                        `data:image/png;base64, ${row.favicon}` : 
                        defaultFavicon
                    }">
                <span style="width: 50px;">${domain}</span>
            </a>`);
        },
        width : '15%'
    },
    {
        name: 'Owner',
        data : row => {
            return html(`
            <div class="text-truncate">${ row.site.UserName }</div>
            <div class="text-truncate">(${ row.site.UserEmail })</div>`);
        },
        width : '15%'
    },
    {
        name: 'Agent',
        data : row => {
            return row.site.AgentName || '-= Unknown =-'
        }
    },
    {
        name     : "Monitor Plan", 
        data      : row => {
            return row.site.MonitorPlanName || 'Free scan';
        }
    },
    {
        name     : "Expiry Date",
        data     : row => {
            let expDate = row.expirationDate;
            let date = formatAsDate( expDate );
            if( date == 'Invalid Date' ) return 'N/A';
            return datePassed( expDate )? 'Expired' : date;
        }
    },
    {
        name     : "Schedule",
        data     : row => {
            let method = row.type;
            return row.site.MonitorPlanName? 'sched' : 'Free scan';
            // return `<span class="sr-only">${utils.dec2bin( method )}</span>
            //         <div class="schedule-method">${methods[ method ]?.text || method}</div>`;
        }
    },
    {
        name     : "Pages",
        data      : row => {
            return formatNumberWithCommas( row.maxPages );
        },
        sort: {
            compare: (a, b) => {
                a = removeCommasFromNumber( a );
                b = removeCommasFromNumber( b );

            if ( a > b) {
                return 1;
              } else if (b > a) {
                return -1;
              } else {
                return 0;
              }
            }
        }
    },
    {
        name     : "Last scan",
        data     : row => formatAsDate( row.lastScan ),
        "data-default-sort" : true
    },
    {
        name     : "Next scan",
        data      : row => {
           return datePassed( row.expirationDate ) ?
                    html('<span class="sr-only">-1</span>Expired') :
                    formatAsDate( row.nextScan );
        }
    },
    {
        name     : "Scan Status",
        data     : row => {
            let scanning = row.isScanning || false;
            if( scanning?.active && scanning.scanType !== ''){
                return html(
                    `<i class="far fa-spinner-third fa-spin"></i> ${
                    scanning.scanType == 'scan'?
                    `Scanning ${scanning.progress.toFixed(1)}%` : 
                    'Mapping'}` );
            }

            return html('<i class="far fa-check-circle"></i> Idle');
        },
    },
    {
        name     : "Activity",
        data     : row => {
            const isActive = row.isActive;
            return html(
                `<span class="sr-only">${Number(isActive)}</span>
                <button 
                    role="switch" 
                    aria-checked="${isActive}" 
                    class="toggle-activity-btn custom-toggle-switch${isActive? ' on' : ''}" 
                    data-bs-toggle="tooltip" title="${isActive? 'Close' : 'Open'} scan"
                    data-key="${row.siteKey}"
                    data-method="toggle-activity"
                >
                    <span class="sr-only">Scan activity</span>
                    <span class="bullet-btn"></span>
                </button>`);
        }
    },
    {
        name     : "Reports",
        data     : row => html(`
                    <button 
                        class="btn view-scans-btn icon-btn" 
                        aria-expanded="false" 
                        title="Show last scans"
                        data-method="view" 
                        data-bs-toggle="tooltip"
                        data-key="${row.siteKey}"
                        data-domain="${row.site.Domain || ''}"
                    >
                        <span class="sr-only">View scans</span>
                        <i class="far fa-chevron-circle-down"></i>
                    </button>`),
        sort: false
    },
    // {
    //     id: 'test',
    //     name: 'Reports',
    //     plugin: {
    //       component: testPlugin,
    //       props: {
    //         id: (row) => row.cell(0).data,
    //       }
    //     },
    //     data : row => row,
    //     sort: {
    //         enabled: false
    //     }
    // },
];
/**
 * 
    {
        name     : "Upgrade",
        data     : row => html(
            `<button 
                class="btn upgrade-plan-btn" 
                data-method="buy-monitor"
                data-key="${row.siteKey}"
            >
            Upgrade 
            ( Non-admin feature )
            </button>`),
        sort: {
            enabled: false
        }
    },

 */