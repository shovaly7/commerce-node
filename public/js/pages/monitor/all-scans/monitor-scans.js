import {
    formatAsDate,
    formatNumberWithCommas
} from "/js/utils/formatters.js";
import {
    cssVar,
    ColorConvert
} from "/js/utils/helpers.js";
import {getA11yProvider} from "/js/utils/a11y-providers.js";
import {Chart, registerables} from '/chart.js/dist/chart.esm.js';
Chart.register(...registerables);

const scanSummary	= {
    errorCount : "General Errors",
    totalHeadings : "Headings",
    totalRole : "Roles",
    totalAria : "ARIA Roles",
    accessKeys : "Access Keys",
    emptyLinks : "Empty Links",
    noLabelFields : "Fields without label",
    noAlts : "Altless Images"
}

const colors = {
    red: cssVar('--ew-red'),
    green: cssVar('--ew-green'),
    blue: cssVar('--ew-blue'),
    darkGreen: cssVar('--ew-dark-green'),
    dark: cssVar('--ew-dark')
}
colors.fades = {
    red : ColorConvert( colors.red, '0.6'),
    green : ColorConvert( colors.green, '0.6'),
    blue : ColorConvert( colors.blue, '0.6')
}

const setPlural = number => number > 1? 's' : '';

const prepareScansTableCellClass = prop =>{
    return `${ prop.toLowerCase().split(' ').join('-') }-cell`;
}

const prepareScanData = (record, i) =>{
    let index = i + 1;
    let evenOrOdd = index % 2 === 0 ? 'even' : 'odd';
    let a11y = record.a11y;
    const isEW = a11y?.version? true : false || false;
    let hasA11y = a11y.exist > 0;
    const provider = isEW? 'EqualWeb' : hasA11y? getA11yProvider( a11y.a11yType ).name : 'No Widget';
    let missingCount = hasA11y? a11y.summary.scanned.total - a11y.exist : -1;

    const {topRowComputed, bottomRowComputed} = renderComputedCells({
        record,
        a11y,
        hasA11y
    });

    let topRowHTML = renderTopRow({
        key : record.siteKey,
        tiny : record.tiny,
        topRowComputed,
        evenOrOdd,
        scanDate : formatAsDate( record.date ),
        provider,
        standard : record.standard,
        scanMode : hasA11y? (isEW? a11y.a11yMode : '- None -') : 'No A11y',
        qPages : formatNumberWithCommas( record.qPages ),
        scannedSuccess : record.summary.scanned.success,
        i
    });

    let bottomRowHTML = renderBottomRow({
        key : record.siteKey,
        bottomRowComputed,
        evenOrOdd,
        hasInstalledA11y : hasA11y && missingCount < 1,
        hasA11y,
        missingCount,
        fixedBy : isEW? 'EqualWeb' : provider,
        showUpgradeBtn : i == 0 && a11y.a11yMode !== 'full',
        i
    });

    return topRowHTML + bottomRowHTML;
}


const renderComputedCells = ({
    record,
    a11y,
    hasA11y
}) =>{
    let topRowComputed = '', 
        bottomRowComputed = '';

    for( let prop in scanSummary ){
        let value = record.summary[prop];

        let tdClass = `class="${ prepareScansTableCellClass( scanSummary[prop] ) }"`;
        if ( value == null ) {
            topRowComputed 	  += `<td ${tdClass}>Unknown</td>`;
            bottomRowComputed += `<td ${tdClass}></td>`;
        }
        else if( typeof value == 'number' ){

            let a11yValue = a11y.summary[prop];
            let delta = a11yValue - value;
            let noChanges =  delta == 0;
            let added = delta > 0;

            topRowComputed += renderNumericCell({
                isTop: true,
                tdClass,
                value,
                a11yValue,
                noChanges,
                hasA11y,
                added
            });

            bottomRowComputed += renderNumericCell({
                isTop : false,
                tdClass,
                noChanges,
                hasA11y,
                added,
                absDelta : Math.abs(delta)
            });
        }
    }

    return {topRowComputed, bottomRowComputed}
}

const renderNumericCell = ({
    isTop,
    tdClass,
    value,
    a11yValue,
    noChanges,
    hasA11y,
    added,
    absDelta
}) =>{
    if( isTop ){
        return `
        <td ${tdClass}>
            <span class="old-value">${value}</span>
            ${!hasA11y || noChanges? '' : 
            `<span class="current-value ${ added? 'text-success' : 'text-danger'}">
                <i class="fa fa-${ added? 'sort-up' : 'sort-down'}"></i>
                ${a11yValue}
            </span>`}
        </td>`.trim();
    } else{

        return hasA11y? `
        <td ${tdClass}>
            <span class="fixed-value">${
                absDelta
            } ${
                noChanges? '' :
                added? 
                    'added' : 
                    'fixed'
            }</span>
        </td>`.trim() : 
        `<td ${tdClass}></td>`;
    }
}

const renderTopRow = ({
    key,
    tiny,
    topRowComputed,
    evenOrOdd,
    scanDate,
    provider,
    standard,
    scanMode,
    qPages,
    scannedSuccess,
    i
}) => {
    return `
    <tr class="top-row ${evenOrOdd}">
        <td class="date-scan-cell">${scanDate}</td>
        <td class="a11y-provider-cell">${provider}</td>
        <td class="a11y-standard-cell">${standard}</td>
        <td class="mode-scan-cell">${scanMode}</td>
        <td class="pages-scan-cell">${qPages}</td>
        <td class="scanned-scan-cell">${scannedSuccess}</td>${
            topRowComputed
        }<td class="actions-scan-cell">
            <a 
                class="btn get-report-btn"
                href="/monitor/report/${key}/${tiny}"
                data-id="${i}"
            >
            View Report
            </a>
            <button 
                class="btn icon-btn delete-report-btn" 
                data-id="${i}"
                data-key="${key}"
                data-method="delete-report"
            >
                <icon></icon>
                <span class="sr-only">Delete Report</span>
            </button>
        </td>
    </tr>`.trim();
}

const renderBottomRow = ({
    key,
    bottomRowComputed,
    evenOrOdd,
    hasInstalledA11y,
    hasA11y,
    missingCount,
    fixedBy,
    showUpgradeBtn,
    i
}) =>{
    return `
    <tr class="bottom-row ${evenOrOdd}">
        <td colspan="6">${
        hasInstalledA11y?
            `<span class="fixed-by-ew">Fixed by ${fixedBy}</span>` : 
            `<span class="text-danger">
                Warning - No accessibility widget installed${
                hasA11y? 
                    ` in ${missingCount} page${setPlural( missingCount )}.` : '!'
                }
            </span>`
        }</td>${
            bottomRowComputed
        }<td class="actions-scan-cell">${
        showUpgradeBtn? 
            `<a 
                class="btn upgrade-plan-btn"
                href="/monitor/plans/${key}"
                data-id="${i}"
            >
                Upgrade Widget
            </a>` : ''
        }</td>
    </tr>`;
}

const renderScansTable = data =>{
    return `
    <table class="scans-table" style="width: 100%;">
        <thead>
            <tr>
                <th class="date-scan-cell">Date</th>
                <th class="a11y-provider-cell">Provider</th>
                <th class="a11y-standard-cell">Standard</th>
                <th class="mode-scan-cell">A11y mode</th>
                <th class="pages-scan-cell">Site pages</th>
                <th class="scanned-scan-cell">Page Scanned</th>${ 
                Object.values( scanSummary ).map( prop => `
                    <th class="${prepareScansTableCellClass( prop )}">${ prop }</th>` 
                ).join('')
                }<th class="actions-scan-cell">Actions</th>
            </tr>
        </thead>
        <tbody>${
            data.map( (record, i) => prepareScanData( record, i ) ).join('')
        }</tbody>
    </table>`;
}

const renderChart = ({
    canvas,
    data,
    title, 
    isEW
}) => {
    const summary = data.summary;
    const a11ySummary = data.a11y.summary;

    const datasets = [
        {
            _id: 'noWidget',
            label: 'No Widget',
            borderColor: colors.red,
            backgroundColor: colors.red,
            hoverBackgroundColor : colors.fades.red,
            fill: false,
            data: [],
        }, {
            _id: 'changedBy',
            label: 'Changed By',
            borderColor: colors.blue,
            backgroundColor: colors.blue,
            hoverBackgroundColor : colors.fades.blue,
            fill: false,
            data: [],
        }, {
            _id: 'newData',
            label: isEW? 'EqualWeb Tool' : 'AI tool',
            borderColor: colors.darkGreen,
            backgroundColor: colors.darkGreen,
            hoverBackgroundColor : colors.fades.green,
            fill: false,
            data: [],
    }].map( (dataset, i) =>{
        dataset.data = [];
        let targetObj = dataset._id == 'noWidget'? 
                summary :
                dataset._id == 'newData'? 
                    a11ySummary : null;
        Object.keys( scanSummary ).forEach( (key, i) =>{
            dataset.data.push( 
                targetObj? 
                    targetObj[key] : 
                    Math.abs(a11ySummary[key] - summary[key]) 
            );
        });
        return dataset;
    });

    return new Chart( canvas.getContext('2d'), {
        type: 'bar',
        data: {
            labels : Object.values( scanSummary ),
            datasets
        },
        options: {
            layout : {
                padding : {
                    left : 20,
                    right : 20,
                    top : 10,
                    bottom : 10
                }
            },
            plugins: {
                legend : {
                    position : 'bottom',
                    labels: {
                        boxWidth : 12,
                        padding : 16
                    }
                },
                title : {
                    text : title,
                    display : true,
                    position : 'top',
                    font : {
                        size : 20,
                        color : colors.dark
                    },
                    padding : 16
                },
            },
            responsive: true,
            maintainAspectRatio : false,
            scales: {
                xAxes: [{
                    gridLines: {
                        drawOnChartArea: false,
                    }
                }],
                yAxes: [{
                    display: true,
                    position: 'left',
                    id: 'y-axis-errors',
                }],
            }
        }
    });
}

export class MonitorScans{
    constructor( domain, data ){
        this.data = data;
        this.domain = domain;
        this.container = document.createElement('scans');
        this.title =  `Scan results - ${ formatAsDate( data[0].date ) }`;
    }
    render(){
        this.container.innerHTML = `
        <div class="chart-container" style="position: relative;">
            <canvas role="img" aria-label="${ this.title }">
        </div>
        <h3>
            Scan results for 
            <a href="http://${ this.domain }" target="_blank" rel="noopener noreferrer nofollow">${
                this.domain
            }</a>
        </h3>${ renderScansTable( this.data ) }`;
        this._chartInst?.destroy();//TODO: check if necessary..
        this._chartInst = renderChart({
           canvas : this.container.querySelector('canvas'),
           title : this.title,
           data : this.data[0],
           isEW : this.data[0].a11y?.version? true : false || false
        });
        return this.container;
    }
}