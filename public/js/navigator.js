( ()=>{
    const commerceOrigin = location.origin;
    const header = document.body.querySelector('header');
    const mainContainer = document.getElementById('main-container');
    const bottomSection = document.getElementById('bottom-section');
    const sideMenu = mainContainer.querySelector('#side-menu');
    const menuLinks = sideMenu.querySelector('#site-links');
    const subMenus = menuLinks.querySelectorAll('.sub-menu');
    const mainContent = mainContainer.querySelector('#page-content');

    const evalAsnycScripts = container =>{
        const createScript = script =>{
            let clone = document.createElement('script');
            clone.type = script.type;
            if( script.src )
                clone.src = `${script.src}?ts=${ Date.now() }`;//unique "ts" to force evaluation..
            else clone.innerHTML = script.innerHTML;
            return clone;
        }
        container.querySelectorAll('script:not([type="application/json"])').forEach( script =>{
            let cloned = createScript( script );
            document.head.appendChild(  cloned ).parentNode.removeChild( cloned );
        });
    }
    const toggleSubMenus = newLink =>{
        let next = newLink.nextElementSibling;
        if( next && next.classList.contains('sub-menu') ){
            subMenus.forEach( sub => sub.hidden = true)
            next.hidden = false;
        }
    }
    const setNewTitle = () =>{
        let h1 = mainContent.querySelector('h1');
        if( h1 ) 
            document.title = `${ h1.innerText.trim() } - EqualWeb`;
    }
    const updateState = newState =>{
        history.pushState(null, null, newState);
    }

    const renderNewContent = html =>{
        mainContent.innerHTML = html;
        evalAsnycScripts( mainContent );
    }
    const getPage = _url =>{
        return new Promise((resolve, reject) =>{
            let url = new URL( _url );

            fetch(`${ url }${ url.search? '&' : '?' }async=true`)
                .then( async response =>{
                    let html = await response.text();
                    if( response.ok ) 
                        return html;
                    throw {
                        code : 404,
                        html
                    }
                })
                .then( html =>{
                    resolve( html );
                })
                .catch( response =>{
                    if( response.code === 404 ){
                        resolve( response.html );
                    } else{
                        reject( response );
                    }
                })
        });
    }
    document.body.addEventListener('click', async (e)=>{
        let link = e.target.closest('a');

        if( link && link.href && !link.target && link.href.startsWith( commerceOrigin ) ){
            let url = link.href;
            e.preventDefault();

            if( url == '') {
                toggleSubMenus( link );
                return;
            }
            if( url == location.href ) 
                return;

            getPage( url )
                .then( renderNewContent )
                .then( updateState( url ) )
                .then( setNewTitle )
                .then( toggleSubMenus( link ) )
                .catch( response =>{
                    debugger;
                })
        }
    });
})();