import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { isLoggedIn } from '../../utils/session.js';
import { isValidPINcode } from '../../utils/santizer.js';
import { createNewUser } from '../../models/user/index.js';
import { flashTempUser, 
         saveUserSession,
         tryToLogin } from '../../utils/session.js';
import * as santizer from '../../utils/santizer.js';
import * as errors from '../../utils/errors.js';
import * as nexmo from '../../middlewares/nexmo/utils.js';
import { sendEmail } from '../../middlewares/mailer/transporter.js';


var router = Router();

/* Activation */
router.all('/', (req, res, next) =>{
  if( isLoggedIn( req ) ){
    return res.redirect('/home');
  }

  if( !req.session.tempUser ){
    return res.redirect('/login');
  }

  next();
});

router.get('/', (req, res, next) =>{
  let tempUser = req.session.tempUser;
  
  render('security/activation', { req, res }, { 
    title: 'Activation',
    maskedEmail : tempUser.maskedEmail,
    maskedPhone : tempUser.maskedPhone,
    hasCode : tempUser.pinCode !== undefined,
    smsSent : tempUser.smsSent || false,
    phoneCallStarted :  tempUser.phoneCallStarted || false,
    emailSent : tempUser.emailSent || false
  });
});

const extractCookies = (req, res, next) =>{
  let {cookies} = req;
  res.locals.cookies = {
    a_aid : santizer.isPositiveInteger( cookies.a_aid )? cookies.a_aid : null,
    a_bid : santizer.isAlphaNumeric( cookies.a_bid )? cookies.a_bid : null,
    agentRef : santizer.isPositiveInteger( cookies.ref )? cookies.ref : null,//what is it?
    ew_referer : santizer.isValidURL( cookies.ew_referer )? cookies.ew_referer : null,//Should we do something with this?
  };
  next();
}

const generatePinCode = (req, res, next) => {
  if( !req.session.tempUser.pinCode ){
    req.session.tempUser.pinCode = nexmo.generatePinCode();
  }
  next();
}

const sendCodeByEmail = (req, res, next) =>{
  if( req.session.tempUser.emailSent )
    return next();

  sendEmail({
    to :  req.session.tempUser.email,
    subject: 'Activate your account',
    content: {
      body: `Please enter the following code to activate your account: ${req.session.tempUser.pinCode}`
    }
  })
    .then( result =>{
      req.session.tempUser.emailSent = true;
      next();
    })
    .catch( err =>{
      debugger;
    })
}

const sendSMS = (req, res, next) => {
  if( req.session.tempUser.smsSent )
    return next();

  nexmo.sendPinBySMS({
      destNumber : req.session.tempUser.phone,
      code : req.session.tempUser.pinCode,
      lang : 'en'
  })
    .then( result =>{
      req.session.tempUser.smsSent = true; 
      debugger;
      next();
    })
    .catch(err =>{
      debugger;
      return errors.MissingOrInvalidParams( res, err.message);
    })
}

const startPhoneCall = (req, res, next) => {
  if( req.session.tempUser.phoneCallStarted )
    return next();
  
  nexmo.sendPinByVoice({
      destNumber : req.session.tempUser.phone,
      code : req.session.tempUser.pinCode,
      lang : 'en'
  })
    .then( result =>{
      req.session.tempUser.phoneCallStarted = true;
      debugger;
      next();
    })
    .catch(err =>{
      debugger;
      return errors.MissingOrInvalidParams( res, err.message);
    })
}

const sendPinCode = (req, res, next) => {
    switch( req.params.type ){
      case 'email':
        sendCodeByEmail( req, res, next );
        break;
      case 'phone':
        startPhoneCall( req, res, next );
        break;
      case 'sms':
        sendSMS(req, res, next);
        break;
      default:
        return errors.NotFound( res, 'Unfimilar activation request');
    }
}

const verifyPinCode = ( req, res, next ) => {
  let inputCode = req.body.code;
  let storedCode = req.session.tempUser.pinCode || null;

  if( !storedCode ){
    return errors.MissingOrInvalidParams( res, 'No code exists. Please try again.' );
  }

  if( !isValidPINcode( inputCode ) ){
    return errors.MissingOrInvalidParams( res, 'Please enter a valid PIN code (6 digits).' );
  }
  
  if( inputCode != storedCode ){
    return errors.MissingOrInvalidParams( res, 'Code does not match. Please try again.' );
  }

  next();
}

const createUser = (req, res, next) => {
  let tempUser = req.session.tempUser;
  let cookies = res.locals.cookies;

  createNewUser({
      name : tempUser.name, 
      email : tempUser.email, 
      phone : tempUser.phone,
      password : tempUser.password,
      country : tempUser.country,
      currencyCode : tempUser.country == 'IL'? 1 : 2,
      a_aid : cookies.a_aid,
      a_bid : cookies.a_bid
  })
      .then( succeed =>{
          if( !succeed ) 
            return errors.Unexpected( res, 'Unexpected error occurred. Please try again.');

          res.locals.email = tempUser.email;
          next();
      })
      .catch( err =>{
          console.error( err.message );
          return errors.Unexpected( res, 'Unexpected error occurred. Please try again.');
      })
}

router.post('/by/:type',
  generatePinCode,
  sendPinCode,
  (req, res ) => {
    res.status( 200 ).json({
      result: 'Code sent successfully'
    });
  }
);

router.post('/check', 
  verifyPinCode,
  extractCookies,
  createUser,
  flashTempUser,
  tryToLogin,
  saveUserSession,
  (req, res ) => {
    return res.redirect('/home');
  }
);

export default router;