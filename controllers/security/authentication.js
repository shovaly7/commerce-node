import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { isValidPINcode } from '../../utils/santizer.js';
import * as errors from '../../utils/errors.js';
import {setLoginHash} from '../../utils/helpers.js';
import * as nexmo from '../../middlewares/nexmo/utils.js';
import {setUserField} from '../../models/user/index.js';
var router = Router();

/* Authentication */
router.all('/', (req, res, next) =>{
  let user = req.session.user;
  if( !user.shouldAuthenticate || user.doubleAuthenticated ) 
    return res.redirect('/home');
  
  next();
})

router.get('/', (req, res, next) =>{
  let user = req.session.user;

  const userName = user.name;
  render('security/authentication', { req, res }, { 
    title: `Authenticate as ${ userName }`,
    userName,
    smsSent : user.smsSent || false,
    requestID : user.requestID || null
  });
});

const generatePinCode = (req, res, next) => {
  if( !req.session.user.pinCode ){
    req.session.user.pinCode = nexmo.generatePinCode();
  }
  
  next();
}

const sendPinCode = (req, res, next) => {
  if( req.session.user.smsSent && req.session.user.phoneCallStarted ){
    return next();
  }

  nexmo.sendPinByBoth({
    destNumber : '+972542503330',
    code : req.session.user.pinCode,
    lang : 'en',//'he'
    sms : !req.session.user.smsSent,
    call : !req.session.user.phoneCallStarted
  })
    .then( results => {
      if( results.sms ){
        if( results.sms.error ){
          console.error( results.sms.error );
        } else req.session.user.smsSent = true;
      }

      if( results.call ){
        if( results.call.error ){
          console.error( results.call.error );
        } else req.session.user.phoneCallStarted = true;
      }
      next();
    })
    .catch( err => {
      console.error( err );
      return errors.Unexpected( res, err.message );
    })
}

const verifyPinCode = ( req, res, next ) => {
  let inputCode = req.body.code;
  let storedCode = req.session.user.pinCode || null;

  if( !storedCode ){
    return errors.MissingOrInvalidParams( res, 'No code exists. Please try again.' );
  }

  if( !isValidPINcode( inputCode ) ){
    return errors.MissingOrInvalidParams( res, 'Please enter a valid PIN code (6 digits).' );
  }
  
  if( inputCode != storedCode ){
    return errors.MissingOrInvalidParams( res, 'Code does not match. Please try again.' );
  }

  next();
}

const updateAuthState = ( req, res, next ) => {
  //Account verfied!
  let email = req.session.user.email;
  setUserField( email, 'loginHash', setLoginHash( req ) )
    .then( result =>{
      console.debug('User verified!', email );
      delete req.session.user.smsSent;
      delete req.session.user.pinCode;
      req.session.user.doubleAuthenticated = true;
      res.redirect('/home');
    })
    .catch( err =>{
      console.error( err );
      return errors.Unexpected( res, err.message );
    })
}

/**
 * TODO: verify that the user has phoneNumber..
 */
router.post('/send', 
  generatePinCode,
  sendPinCode,
  (req, res ) => {
    return res.status( 200 ).send({ result : 'Code sent..' });
  }
);

router.post('/check',
  verifyPinCode,
  updateAuthState
);
export default router;