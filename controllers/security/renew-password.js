import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { isValidPassword } from '../../utils/santizer.js';
import md5Hash from '../../utils/md5-hash.js';
import {extendPasswordExpiration, setNewPassword} from '../../models/user/index.js';

var router = Router();

/* Renew Password*/
router.get('/', (req, res, next) =>{
  render('security/renew-password', { req, res }, { 
    title: 'Renew your password'
  });
});

router.get('/remind-me-later', (req, res, next) =>{
  extendPasswordExpiration( req.session.user.email );
  res.status( 200 ).send('ok');
});

router.post('/', async (req, res, next) => {
  //validate the new password format
  let newPassword = req.body.password;
  if( !isValidPassword( newPassword ) )
    return res.status( 400 ).send({ "error": "Invalid password" });
  
  let hashed = md5Hash( newPassword );
  
  //save it
  setNewPassword(req.session.user.email, hashed )
    .then( passChanged =>{
      req.session.user.passExpired = false;
      return res.redirect('/home');
    })
    .catch( err =>{
      return res.status( 400 ).send('Unexpected error occurred. Try again later.');
    })
});
export default router;