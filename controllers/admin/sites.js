import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { getAllSites } from '../../models/admin/site.js';

var router = Router();

/* All admin sites */
router.get('/', (req, res, next) =>{
render('admin/sites', { req, res }, { 
    title: 'All Sites'
    });
});

router.post('/', (req, res, next) =>{
    getAllSites(req.query.limit, req.query.offset )
        .then( results => {
            return res.status(200).send( results );
        })
        .catch(err =>{
            debugger;
            return res.status(500).send( err );
        })
});

export default router;