import { Router } from 'express';
import { render } from '../../utils/renderer.js';

var router = Router();

/* Admin dashboard */
router.get('/', (req, res, next) =>{
    render('admin/dashboard', { req, res }, { 
        title: 'Dashboard'
        });
});

export default router;