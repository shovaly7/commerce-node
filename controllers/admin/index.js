import { Router } from 'express';
// import { render } from '../../utils/renderer.js';
import sites from './sites.js';
import dashboard from './dashboard.js';
import logger from '../../boot/logger.js';

var router = Router();

/* logged.. */
router.get('*', (req, res, next) =>{
  if( req.session.user.role !== 'ADMIN' ){
      logger.warn('Non-ADMIN user try to access!');
      return res.status( 404 ).send('Not found');
  }
  next();
});

router.get('/', (req, res, next) =>{
  return res.redirect('/home');
});

router.use('/sites', sites);
router.use('/dashboard', dashboard);

export default router;