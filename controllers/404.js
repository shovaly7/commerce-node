import { Router } from 'express';
import { render } from '../utils/renderer.js';

var router = Router();

/* 404 */
router.all( '*', (req, res ) =>{
    console.error('Route not found:', req.method, req.originalUrl );
    let error = new Error('Page not found');
    error.code = error.status = 404;

    res.status( error.code );
    
    return render('error', { req, res }, {
      title: `Error - Page not found`,
      error
    });
});

export default router;