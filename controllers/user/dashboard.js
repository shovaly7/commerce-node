import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { getSiteStats } from '../../models/site/index.js';
var router = Router();

/* User Dashboard */
router.get('/', (req, res, next) =>{
  render('user/dashboard', { req, res }, { 
    title: 'Dashboard'
  });
});

const fetchSiteStats = (req, res, next) =>{
  getSiteStats( req.session.user.id )
    .then( stats =>{
        res.locals.stats = stats;
        next();
    })
    .catch(err =>{
        res.locals.stats = null;
        console.error(err);
    })
}

const findPopularWidget = ( req, res, next )=>{
  let stats = res.locals.stats && res.locals.stats[0] || false;
  let result = {};
  if( stats ){
    delete stats['lang'];
    let max;
    let maxName;
    for( const mode in stats ){
      let count = +stats[mode];
      if( !max || count > max ){
         max = count;
         maxName = mode;
      }
    }
    if( max > 0 && maxName ){
      result.count = max;
      result.name = maxName;
    }
  }
  req.session.user.popularWidget = JSON.stringify( result );// result;
  if( result )
    return res.status( 200 ).json( result ); 
  return res.status( 404 ).json({});
}

const getMaxPopularStats = (req, res, next) =>{
  if( req.session.user.popularWidget )
    return res.status(200).json( JSON.parse( req.session.user.popularWidget ) );
  next();
}


router.get('/popular-widget', 
    getMaxPopularStats,
    fetchSiteStats,
    findPopularWidget
  )

export default router;