import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { isLoggedIn } from '../../utils/session.js';
import dashboard from './dashboard.js';
var router = Router();

/* logged.. */
router.all('/', (req, res, next) =>{
  // if user is unAuthenticated in the session, redirect him to login..
  if( !isLoggedIn( req ) ){
    res.redirect('/login');
    return;
  }
  next();
});

router.all('/dashboard/', dashboard); 

export default router;