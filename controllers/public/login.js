import { Router } from 'express';
import md5Hash from '../../utils/md5-hash.js';
import { render } from '../../utils/renderer.js';
import * as errors from '../../utils/errors.js';
import {isValidPassword, isValidEmail, formatPhoneNumber} from '../../utils/santizer.js';
import { setLoginHash } from '../../utils/helpers.js';
import { saveUserSession, tryToLogin } from '../../utils/session.js';
import {updateLastLoginTime} from '../../models/user/index.js';

var router = Router();

/* login */
router.get('/', (req, res, next) =>{
  render('public/login', { req, res }, { 
    title: 'Login'
  });
});

const checkEmailAndPasswordValidity = (req, res, next) =>{
  let {email, password} = req.body;
  email = email.toLowerCase().trim();

  if( !email || !password )
    return errors.MissingOrInvalidParams( res, 'Missing or invalid email or password' );
  
  if( !isValidEmail( email, 100 ) || !isValidPassword( password ) )
    return errors.MissingOrInvalidParams( res, 'Missing or invalid email or password' );

  res.locals = {email, purePassword : password}; 
  next();
}

const checkIfUserLocked = (req, res, next) =>{
  if( res.locals.user.isLocked ){
    //User is locked..
    return errors.ResourceLocked(res, 'Some of your credentials are incorrect.\n Please contact our support team.')
  }
  next();
}


const verifyUserPassword = (req, res, next) =>{
  if( res.locals.user.password !== md5Hash( res.locals.purePassword ) ){
    return errors.MissingOrInvalidParams( res, 'Wrong password. Please try again.');
  }
  next();
}

const updateLoginTime = (req, res, next) => {
  updateLastLoginTime( res.locals.email )
    .then( succeed =>{
        next();
    })
    .catch( err =>{
      console.error( err );
      return errors.Unexpected( res );
    })
}

/**
 * Checks whether the account should authenticate with its phone number.
 * - If the account require 2FA, then
 *   -> IF last login hash (based on user-agent and IP) has changed, then
 *   -> Check the account phone number to authenticate with.
 *   -> then go to next step..
 * - otherwise -> go to the next step..
 * @param {object} req 
 * @param {object} res 
 * @param {object*} next 
 * @returns next middleware or error..
 */
const checkIfNeedTFA = (req, res, next) =>{
  let {hasTwoFactorAuth, loginHash, userPhone, userCountry} = res.locals.user;
  res.locals.user.userCountry = userCountry = userCountry && userCountry.toUpperCase();

  res.locals.user.shouldAuthenticate = 
      hasTwoFactorAuth == 'true' && loginHash !== setLoginHash( req ) || false;
  if( res.locals.user.shouldAuthenticate ){
    let phoneNumber = formatPhoneNumber( userPhone, userCountry );
    if( !phoneNumber ){
      return errors.MissingOrInvalidParams( res, 'Missing or invalid phone number' );
    }
    res.locals.user.userPhone = phoneNumber;
  }
  next();
}

router.post('/', 
  checkEmailAndPasswordValidity,
  tryToLogin,
  checkIfUserLocked,
  verifyUserPassword,
  updateLoginTime,
  checkIfNeedTFA,
  saveUserSession,
  ( req, res ) => {
    //Login succeed..
    return res.redirect('/home');
  }
);

export default router;