import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { randomBytes } from 'crypto';
import {sendEmail} from '../../middlewares/mailer/transporter.js';
import tokenService from '../../middlewares/tokens/pass-tokens.js';
import {isUserExist} from '../../models/user/index.js';
var router = Router();

/* Forgot Password*/
router.get('/', (req, res, next) =>{
  render('public/forgot-password', { req, res }, { 
    title: 'Forgot Password'
  });
});

router.post('/', async (req, res, next) => {
  let email = req.body.email;
  if( !isUserExist( email ) ){
    req.flash('error', 'No account with that email address exists.');
    return res.redirect('/forgot-password');
  }

  tokenService.saveToken( randomBytes(20).toString('hex'), email );

  let sent = await sendEmail({
    subject: 'Reset your password',
    content: {
      body: `
        You are receiving this because you (or someone else) have requested the reset of the password for your account.
        Please click on the following link, or paste this into your browser to complete the process:
        http://${req.headers.host}/reset/${token}
        If you did not request this, please ignore this email and your password will remain unchanged.`
    }
  });
  
  req.flash('info', `An e-mail has been sent to ${email} with further instructions.`);

  res.redirect('/forgot-password');
});
export default router;