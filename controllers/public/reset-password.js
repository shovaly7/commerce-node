import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { isValidPassword } from '../../utils/santizer.js';
import md5Hash from '../../utils/md5-hash.js';
import {sendEmail} from '../../middlewares/mailer/transporter.js';
import tokenService from '../../middlewares/tokens/pass-tokens.js';
import {setNewPassword} from '../../models/user/index.js';
var router = Router();

router.all('/:token', (req, res, next) =>{
  if( req.method !== 'GET' && req.method !== 'POST'){
    req.flash('error', 'Password reset token is invalid or has expired.');
    return res.status( 405 ).send({"error" : "Method not allowed"});
  }

  let data = tokenService.getDataByToken( req.params.token );
  if (!data || data.expiration < Date.now()) {
    req.flash('error', 'Password reset token is invalid or has expired.');
    return res.redirect('/forgot-password');
  }
  res.locals.email = data.email;
  next();
});


router.get('/:token', (req, res ) =>{
  render('public/reset-password', { req, res }, { 
    title: 'Reset Password',
    token : req.params.token
  });
});
  
router.post('/:token', async (req, res) => {
  //validate the new password format
  let newPassword = req.body.password;
  if( !isValidPassword( newPassword) )
    return res.status( 400 ).send({ "error": "Invalid password" });
  
  let hashed = md5Hash( newPassword );
  //save it
  setNewPassword(res.locals.email, hashed )
    .then( passChanged =>{
      //reset token cache
      tokenService.deleteToken( req.params.token );
        sendEmail({
          subject: 'Your password has been changed',
          content: {
            body: `This is a confirmation that the password for your account "${res.locals.email}" has just been changed.`
          }
        });
        req.flash('success', `Success! Your password has been changed.`);
      
        return res.redirect('/login');
    })
    .catch( err =>{
      return res.status( 400 ).send('Unexpected error occurred. Try again later.');
    });
});  
export default router;