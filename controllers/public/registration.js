import { Router } from 'express';
import md5Hash from '../../utils/md5-hash.js';
import { render } from '../../utils/renderer.js';
import * as errors from '../../utils/errors.js';
import * as santizer from '../../utils/santizer.js';

import { saveTempUserSession } from '../../utils/session.js';
import {isUserExist} from '../../models/user/index.js';
import {checkRecaptcha} from '../../middlewares/recaptcha/recaptcha-v3.js';

var router = Router();

/* registration */
router.get('/', (req, res, next) =>{
  render('public/registration', { req, res }, { 
    title: 'Registration'
  });
});

const checkFieldsValidity = (req, res, next) =>{
  let {name, phone, email, password} = req.body;

  if( !name || !phone || !email || !password)
    return errors.MissingOrInvalidParams( res, 'Missing or invalid parameter' );

  name = name.trim();
  phone = phone.trim();
  email = email.toLowerCase().trim();

  if( !santizer.isValidEmail( email, 100 ) )
    return errors.MissingOrInvalidParams( res, 'Invalid Email!' );
  
  if( !santizer.isValidPassword( password, 50 ) )
    return errors.MissingOrInvalidParams( res, 'Invalid Password!' );

  let formattedName = santizer.formatName( name, 100 );
  let formattedPhone = santizer.formatPhoneNumber( phone );

  if( !formattedName )
    return errors.MissingOrInvalidParams( res, 'Invalid Name!' );

  if( !formattedPhone )
    return errors.MissingOrInvalidParams( res, 'Invalid Phone!' );

  res.locals.user = {
    email, 
    password : md5Hash( password ), 
    name : formattedName, 
    phone : formattedPhone
  }; 
  next();
}

const checkIfUserExist = ( req, res, next ) => {
  isUserExist( res.locals.user.email )
    .then( exist =>{
      if( exist )
        return errors.ResourceAlreadyExists( res, 'E-mail already exists. Please try to login or reset the password.' ); 
      
      next();
    })
    .catch( err => {
      console.error( err );
      return errors.Unexpected( res );   
    });
}

router.post('/', 
  checkRecaptcha,
  checkFieldsValidity,
  checkIfUserExist,
  saveTempUserSession,
  ( req, res ) => {
    //Registration details saved in session.. try to activate the account..
    return res.redirect('/activation');
  }
);

export default router;