import { Router } from 'express';
// import createError from 'http-errors';
// import * as errors from '../utils/errors.js';
import { isLoggedIn, logOut } from '../utils/session.js';

import routeNotFound from './404.js';
import login from './public/login.js';
import registration from './public/registration.js';
import forgotPassword from './public/forgot-password.js';
import resetPassword from './public/reset-password.js';

import renewPassword from './security/renew-password.js';
import authentication from './security/authentication.js';
import activation from './security/activation.js';

import loggedUser from './user/index.js';
import monitor from './monitor/index.js';

import admin from './admin/index.js';
import logger from '../boot/logger.js';
var router = Router();

/* GET home page. */
router.all('/', logOut, (req, res, next) =>{
  return res.redirect('/registration');
});

router.use('/login', logOut, login); 
router.use('/logout', logOut, (req, res) =>{
  return res.redirect('/login');
});

router.use('/registration', logOut, registration);
router.use('/forgot-password', logOut, forgotPassword);
router.use('/reset', logOut, resetPassword);
router.use('/activation', activation);

router.use('/', ( req, res, next ) => {
  // if user is unAuthenticated in the session, redirect him to login..
  if( req.session.tempUser ){
    return res.redirect('/activation');
  }
  if( !isLoggedIn( req ) ){
    return res.redirect('/login');
  }
  next();
});

///////////////////////////////////////////////////////////////
// === All routers from here on, is already authenticated!! ===
///////////////////////////////////////////////////////////////
router.use('/admin/', admin); 

router.use('/home*', (req, res, next) =>{
  let user = req.session.user;
  if( !user.activated ){
    return res.redirect('/activation');
  }

  if( user.passExpired ){
    return res.redirect('/renew-password');
  }

  if( user.shouldAuthenticate && !user.doubleAuthenticated ) {
    return res.redirect('/authentication');
  }

  switch( user.role ){
    case 'ADMIN':
      return res.redirect('/admin/dashboard');
    case 'AGENT':
      return res.redirect('/agent/dashboard');
    case 'PM':
      return res.redirect('/manager/dashboard');
    default:
      return res.redirect('/dashboard');
  }
});

router.use('/authentication', authentication);
router.use('/renew-password', renewPassword);

router.use('/monitor/', monitor); 
router.use( loggedUser );

// catch 404 and forward to error handler
router.use( routeNotFound );

logger.info('|| Routers set.. ||');
export default router;