import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import AdminMonitorAPI from '../../middlewares/monitor/admin.js';
import allSites from '../../boot/cache/sites.js';

var router = Router();

/* All scans */
router.get('/', (req, res, next) =>{
	render('monitor/all-scans', { req, res }, { 
		title: 'All scans'
	});
});

/**
 * Merge the current schedules from monitor API with local DB settings, based on the same siteKey
 * @param {Array<object>} schedules 
 * @returns {Array<object>} merged filtered schedules
 */
const mergeSitesWithSchedules = schedules =>{
	schedules = schedules
		.map( schedule =>{
			let site = allSites.getSite( schedule.siteKey );
			if( site ){
				schedule.site = site;
				return schedule;
			}
			return null;
			
		})
		.filter( Boolean );
	return schedules;
}

const adminCall = req => new AdminMonitorAPI( req.session.user.email );

router.post('/data/', (req, res, next ) =>{
	const {limit, offset} = req.query;
	let api = adminCall( req );
	api.getAllSchedules(offset, limit)
		.then( schedules =>{
			res
				.status( 200 )
				.json( 
					schedules? 
						mergeSitesWithSchedules( schedules ) : 
						[]
				);
		})
		.catch(err =>{
			res.status( err.code || 500 ).json([]);
		});
});

router.post('/data/:siteKey', (req, res ) =>{
	// let api = adminCall( req );
	adminCall( req )
		.getScansOfSite( req.params.siteKey )
		.then( scans =>{
			res.status( 200 ).json( scans );
		})
		.catch( err =>{
			// debugger;
			return res.status( 404 ).json([]);
		});
})

export default router;