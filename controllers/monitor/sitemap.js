import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { validateSiteKey } from '../../middlewares/validators/index.js';
import MonitorAPI from '../../middlewares/monitor/client.js';
var router = Router();

/* SiteMap */
router.get('/:siteKey', validateSiteKey, ( req, res, next) =>{
  let siteKey = req.params.siteKey;

  render('monitor/sitemap', { req, res }, { 
    title: 'SiteMap',
    siteKey
  });
});

// router.post('/:siteKey', (req, res, next) =>{
// //   let api = new MonitorAPI({ 
// //     userID : req.session.user.id,
// //     caller : req.session.user.email
// //   });
// //   api.getScansDates( req.params.siteKey )
// });

export default router;