import { Router } from 'express';
import { render } from '../../utils/renderer.js';

import report from './report.js';
import allScans from './all-scans.js';
import plans from './plans.js';
import sitemap from './sitemap.js';

var router = Router();

//Monitor zone..

router.get('/', (req, res, next) =>{
    return res.redirect('/monitor/home');
});

router.get('/home', (req, res, next) =>{
    render('monitor/home', { req, res }, { 
        title: 'WCAG monitor'
    });
});

router.use('/all-scans/', allScans);
router.use('/report/', report);
router.use('/plans/', plans);
router.use('/sitemap/', sitemap);

export default router;