import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import MonitorAPI from '../../middlewares/monitor/client.js';
import { validateSiteKey } from '../../middlewares/validators/index.js';
import { isAlphaNumeric, isValidDate } from '../../utils/santizer.js';
import * as errors from '../../utils/errors.js';
import logger from '../../boot/logger.js';
var router = Router();

const reportPageRoot = `/monitor/report/`;

/* Monitor Report form*/
router.get('/', ( req, res ) =>{
  render('monitor/report', { req, res }, { 
		title: 'Monitor Report',
    siteKey : null,
    reportID : null
	});
});

/* Monitor report of site*/
router.get('/:siteKey', validateSiteKey, ( req, res, next) =>{
    //Go get last scan ID..
  let api = new MonitorAPI({ 
    userID : req.session.user.id,
    caller : req.session.user.email
  });
  
  api.getSiteLastScanID( req.params.siteKey )
        .then( lastScan =>{
          //Last scan exist? redirect to it.. otherwise redirect to main report page..
          res.redirect( 
            lastScan?.tiny? 
              `/monitor/report/${ req.params.siteKey }/${ lastScan.tiny }` :
              '/monitor/report' )
        })
        .catch( response =>{
          logger.error( response.message || response );
          res.redirect( '/monitor/report' );
        });
});

/* Monitor specific report */
router.get('/:siteKey/:id', validateSiteKey, ( req, res ) =>{
  let {siteKey, id} = req.params;

	if( !isAlphaNumeric( id ) ){
    return errors.MissingOrInvalidParams( res, 'Bad ID');
  }
      
	render('monitor/report', { req, res }, { 
		title: 'Monitor Report',
		siteKey,
    reportID : id
	});
})

// router.post('/scans/:siteKey', (req, res, next) =>{
//   let api = new MonitorAPI({ 
//     userID : req.session.user.id,
//     caller : req.session.user.email
//   });
//   api.getScansDates( req.params.siteKey )
// });

export default router;