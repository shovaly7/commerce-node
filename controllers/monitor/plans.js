import { Router } from 'express';
import { render } from '../../utils/renderer.js';
import { validateSiteKey } from '../../middlewares/validators/index.js';
import allPlans from '../../boot/cache/plans.js';
var router = Router();

/* Monitor plans page */
router.get('/:siteKey', validateSiteKey, ( req, res, next) =>{
	let siteKey = req.params.siteKey;
		
	render('monitor/plans', { req, res }, { 
		title: 'Monitor Plans',
		siteKey
	});
})

// router.get('/data/', (req, res ) =>{
//     res
//         .status( 200 )
//         .json( allPlans.MonitorPlans );
// });

export default router;