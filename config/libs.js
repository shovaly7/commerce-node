//The following array includes all dependencies libs for the application..
export default [
    'bootstrap', 
    {'popperjs/core/' : '@popperjs/core/'},
    'gridjs',
    'chart.js',
    'recaptcha-v3'
]; 