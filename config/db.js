export default {
  user: process.env.db_user,
  password: process.env.db_password,
  database: process.env.db_name,
  server: process.env.server_host,
  domain: process.env.server_host,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000
  },
  options: {
    encrypt: false, // for azure
    trustServerCertificate: true // change to true for local dev / self-signed certs
  }
}