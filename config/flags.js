export const isDev = process.env.node_env == 'development';
export const isDevTestMode = isDev && process.env.test_mode == 'true';
export const hasSiteProcess = process.env.has_site_process == 'true';
export const a11yVersion = process.env.a11y_version || '4.0.0';
export const isCDK = process.env.is_cdk  == 'true';
