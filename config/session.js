import {isDev} from './flags.js';
/**
 * TODO:
 * Warning: The default server-side session storage, MemoryStore, 
 * is purposely not designed for a production environment. 
 * It will leak memory under most conditions, 
 * does not scale past a single process, 
 * and is meant for debugging and developing.
 * For a list of stores, see "compatible session stores"
 * https://www.npmjs.com/package/express-session#compatible-session-stores
 */
export default { 
    secret: process.env.session_secret,
    resave: false, 
    saveUninitialized: false,
    name : 'sid',
    cookie:{
        sameSite : 'strict',
        secure : !isDev,
        maxAge: 60 * 1000 * 20 //20 min.
    }
}