import logger from './boot/logger.js';
import envars from './boot/env-vars.js';
import express,{json, urlencoded} from 'express';
import session from 'express-session';
import useragent from 'express-useragent';
// import flash from 'req-flash';
import { join } from 'path';
import favicon from 'serve-favicon';
import cookieParser from 'cookie-parser';
// import { render } from './utils/renderer.js';
import indexRouter from './controllers/index.js';

var app = express();

app.enable('trust proxy');

//Statics
import staticOpts from './config/static-files.js';
app.use(express.static('public', staticOpts));

import nm_dependencies from './config/libs.js';
nm_dependencies.forEach( lib => {
  let path, libName;

  if( typeof lib === 'string') {
    path = libName = lib;
  } else{
    path = Object.keys( lib )[0];
    libName = lib[ path ];
  }
  
  app.use(`/${path}`, express.static(`./node_modules/${libName}`) );
});

//Configure session middleware
import sess from './boot/session-store.js';
app.use( session( sess ) );

// view engine setup
app.set('views', join('./', 'views'));
app.set('view engine', 'ejs');
app.use(favicon('./public/img/logos/favicon.ico'));

// app.use(logger('dev'));
app.use(json());
app.use(urlencoded({ extended : true  }));
app.use(cookieParser());

// app.use(flash());
app.use(useragent.express());

//First Loggin router..
app.use((req, res, next) =>{
  var msgs = req.session.messages || [];
  res.locals.messages = msgs;
  res.locals.hasMessages = !! msgs.length;
  req.session.messages = [];

  logger.debug(req.method, '==>', req.originalUrl );
  next();
});

//Main root handler
app.use('/', indexRouter);

// error handler
app.use((err, req, res, next) => {
  err.statusCode = err.statusCode || err.code || 500;
  err.status = err.status || 'error';

  logger.error( 'Error: ', err.message );
  if( !res.headersSent )
    res.status( err.statusCode ).json({
      status: err.status,
      message: err.message
    });
});

//Save all sites from DB to cache (Admin usage only..)
import appCache from './boot/cache/app-cache.js';

const port = process.env.PORT || 3000;
app.listen(port, () => {
  logger.info(`Commerce listening at http://localhost:${port}`);
});