import {dbRequest, dbErrorHandler} from '../../boot/db.js';
import {
    columns, 
    table
} from './model.js';
import { SELECT } from '../query-builder.js';

/**
 * Get all plans to save in Cache.
 * @returns {promise<Array>}  All plans in DB
 */
export const getAllPlans = async () => {
    return dbRequest(
		new SELECT()
			.ALL( columns )
			.FROM( table )
			.ORDER_BY( columns.type )
			.ORDER_BY( columns.order )
			.EXEC()
	)
        .then( result =>{
            return result.recordset;
        })
        .catch( err => dbErrorHandler( err ));
}