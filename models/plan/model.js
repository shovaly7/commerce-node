import { setTableNameToColumn } from '../utils.js'
export const table = 'Product';
export const columns = {
	id: {
		colName: "ID",
		type: "integer"
	},
    order : {
        colName: "ItemNumber",
        type: "integer",
        alias : "order"
    },
	type: {
		colName: "ParentID",
		type: "integer",
        alias : "type"
	},
	name : {
		colName: "Name",
		type: "string",
        alias : "PlanName"
	},
    maxPages: {
        colName: "QtyAvail",
        type: "integer",
        alias : "maxPages"
    },
    price: {
        monthly : {
            colName: "ListPrice",
            type: "decimal",
            alias : "monthlyPrice"
        },
        annually : {
            colName: "SalePrice",
            type: "decimal",
            alias : "annuallyPrice"
        },
        oneTime : {
            colName: "quantity",
            type: "integer",
            alias : "oneTimeCost"
        }
    },
    description : {
        colName: "OnSaleMessage",
        type: "text",
        alias : "description"
    },
    text : {
        colName: "Attribute0",
        type: "string",
        alias : "text"
    },
    a11yCoverage : {
        colName: "Description",
        type: "text",
        alias : "a11yCoverage"
    }
	
}
setTableNameToColumn( table, columns );