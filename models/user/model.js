import { setTableNameToColumn } from '../utils.js';
export const table = 'Shopper';
export const columns = {
    id: {
        colName: "ID",
        type: "integer"
    },
    name: {
        colName: "Name",
        type: "string",
        alias: "UserName"
    },
    email: {
        colName: "Email",
        type: "string",
        alias: "UserEmail"
    },
    cellphone: {
        colName: "Phone",
        type: "string",
        alias: "UserCellphone"
    },
    activated: {
        colName: "info1",
        type: "string",
        alias: "Activated"
    },
    isLocked: {
        colName: "Disabled",
        type: "bit",
        alias: "IsLocked"
    },
    password: {
        colName: "Password",
        type: "string"
    },
    hasTwoFactorAuth: {
        colName: "AddressLine2",
        type: "string",
        alias: "Activated"
    },
    loginHash: {
        colName: "AddressLine3",
        type: "string",
        alias: "LoginHash"
    },
    passExpiration: {
        colName: "Pas_Expired",
        type: "string",
        alias: "PassExpiration"
    },
    country: {
        colName: "Country",
        type: "string"
    },
    agentID: {
        colName: "Account_bookkeeping",
        type: "integer",
        alias: "AgentID"
    },
    discount :{
        colName: "Payment_Discount",
        type: "integer",
        alias: "UserDiscount"
    },
    poweredByUrl :{
        colName: "www",
        type: "string",
        alias: "PoweredByURL"
    },
    payment: {
        type: {
            colName: "payment_type",
            type: "integer",
            alias: "PaymentType"
        },
        currency: {
            colName: "Payment_Currency",
            type: "integer",
            alias: "PaymentCurrency"
        }
    }
}

setTableNameToColumn( table, columns);