import validateNonEmpty, {dbRequest, dbErrorHandler} from '../../boot/db.js';
import moment from 'moment';

const approvedFields = {
    "name" : "Name",
    "phone" : "Phone",
    "coutry" : "Country",
    "loginHash" : "AddressLine3",
    "companyName" : "Company",
    "companyPhone" : "Attribute1",
    "companyAddress" : "AddressLine1",
    "companyWebsite" : "Attribute3",
    "invoiceEmail" : "Attribute4"
}

export const logIn = async (email = validateNonEmpty('E-mail') ) => {
    return dbRequest(`
    SELECT 
        S1.ID as 'userID',
        S1.Name as 'userName',
        S1.Email as 'userEmail',
        S1.Phone as 'userPhone',
        S1.info1 as 'activated',
        S1.Disabled as 'isLocked',
        S1.Password as 'password',
        S1.AddressLine2 as 'hasTwoFactorAuth',
        S1.AddressLine3 as 'loginHash',
        S1.Pas_Expired as 'passExpiration',
        S1.payment_type 'paymentType',
        S1.Country 'userCountry',
        C1.code 'paymentCurrency',
        A2.IsSystemAdmin,
        A2.IsProjectsManager,
        A2.IsSalesManager,
        A3.BookingID,
        A3.OwnerID,
        A3.Email as 'agentEmail',
        A3.Name as 'agentName'
    FROM Shopper S1
    INNER JOIN Accessibility_Permissions A2 ON S1.ID = A2.UserID
    LEFT JOIN Agents A3 ON S1.Account_bookkeeping = A3.BookingID
    LEFT JOIN currency C1 ON S1.Payment_Currency = C1.pciCode
    WHERE S1.Email = '${ email }'`)
        .then( result =>{
            return result.recordset.length && result.recordset[0] || false;
        })
        .catch( err => dbErrorHandler( err ));
}

export const isUserExist = async ( email = validateNonEmpty('E-mail') ) => {
    return dbRequest(`SELECT ID FROM Shopper WHERE Email = '${ email }'`)
            .then( result =>{
                return result.recordset.length? true : false;
            })
            .catch( err => dbErrorHandler( err ));
}

export const setNewPassword = async (email = validateNonEmpty('E-mail'), password) => {
    return dbRequest(`
    Update Shopper
        Set Password = '${ password }',
            Pas_Expired = '${ moment().add(1, 'year').toDate().getTime() }'
    WHERE Email = '${ email }'`)
            .then( result =>{
                let rowsAffected = result.rowsAffected;
                return rowsAffected? true : false;
            })
            .catch( err => dbErrorHandler( err ));
}

/**
 * Create new user in DB, and then init its permissions..
 * @param {string} name Full name
 * @param {string} email E-mail
 * @param {string} phone Cellphone number
 * @param {string} password Hashed password
 * @param {string} country Country code
 * @returns {boolean} succeed
 */
export const createNewUser = async ({
    name = validateNonEmpty('Full name'),
    email = validateNonEmpty('E-mail'),
    phone = validateNonEmpty('Cellphone number'),
    password = validateNonEmpty('Password'),
    country,
    currencyCode,
    a_aid,//@@cookie$a_aid (Numbers only)
    a_bid//@@cookie$a_bid (alphanumeric only)
} = {}) => {

    return dbRequest(`
    BEGIN TRANSACTION
        DECLARE @UserID int;

        INSERT INTO Shopper ( WebID, HostMasterID, protype, Admin, HostMasterAdanin, HostMasterSuperAdanin,
            Disabled, SMS, Designer, Account_ID,payment_type, Pas_Expired, info1, sumtopay,
            paymenttaype, proend, Payment_Currency, Created, Name, Email, Phone, Password, Country )
        VALUES ( 5905, 5905, 1, 0, 0, 0, 0, 0, 0, 0, 1,
            '${ moment().add(6, 'months').toDate().getTime()  }', 'activated', 
            '${ a_aid || ''}', '${ a_bid || ''}',
            '${ moment().add(1, 'year').toJSON() }', ${ currencyCode || 2 }, '${ new Date().toJSON() }',
            '${ name }', '${ email }', '${ phone }', '${ password }',  '${ country || '' }' );

        SET @UserID = SCOPE_IDENTITY();

        INSERT INTO Accessibility_Permissions (UserID, UserEmail, ExternalUser)
        Values (@UserID, '${ email }', 'fish');
    COMMIT`)
            .then( result =>{
                let rowsAffected = result.rowsAffected;
                return rowsAffected? true : false;
            })
            .catch( err => dbErrorHandler( err ));
}

/**
 * Update premitted user field by email
 * @param {string} email 
 * @param {string} field 
 * @param {string} value
 * @returns 
 */
export const setUserField = async (email = validateNonEmpty('E-mail'), field, value) => {
    let fieldName = approvedFields[ field ];
    if( !fieldName ) 
        return dbErrorHandler( new Error( `Invalid field ${field}`) );
    return dbRequest(`
    Update Shopper
        Set ${fieldName} = '${ value }'
    WHERE Email = '${ email }'`)
            .then( result =>{
                let rowsAffected = result.rowsAffected;
                return rowsAffected? true : false;
            })
            .catch( err => dbErrorHandler( err ));
}


export const updateLastLoginTime = async (email = validateNonEmpty('E-mail')) => {
    return dbRequest(`
    Update Shopper
        Set LastLogin = '${ new Date().toJSON() }'
    WHERE Email = '${ email }'`)
            .then( result =>{
                let rowsAffected = result.rowsAffected;
                return rowsAffected? true : false;
            })
            .catch( err => dbErrorHandler( err ));
}

export const extendPasswordExpiration = async (email = validateNonEmpty('E-mail'))=> {
    return dbRequest(`
    Update Shopper
        Set Pas_Expired = '${ moment().add(3, 'months').toDate().getTime() }'
    WHERE Email = '${ email }'`)
            .then( result =>{
                let rowsAffected = result.rowsAffected;
                return rowsAffected? true : false;
            })
            .catch( err => dbErrorHandler( err ));
}