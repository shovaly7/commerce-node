import * as utils from './utils.js';
import { isPositiveInteger } from '../utils/santizer.js';
import { isDev } from '../config/flags.js';

/**
 * Compose SQL SELECT statment with all necessary operators and tools..
 */
export class SELECT {
    constructor( settings = { log : false } ) {
        this.log = settings.log || false;
        this.mainTbl = null;
        this.orderBy = [];
        this.constraints = null;
        this.cols = [];
        this.joins = [];
        return this;
    }
    ALL( columns ){
        if( typeof columns == 'object' )
            this.columns( utils.prepareColumnStructure( columns ) );
        else this.cols = '*';
        return this;
    }
    TOP( rows ){
        if( rows && isPositiveInteger( rows ) ){
            this.top = rows;
        }
        return this;
    }
    columns( columns ){
        this.cols.push(
            columns.map( col =>{
                const colName = utils.formatTableWithColumn( col );
                return col.alias? `${ colName } as '${ col.alias }'` : colName;
            }).join(', ') 
       );
       return this;
    }
    FROM( table ){
        this.table = utils.santizeLiterals( table );
        return this;
    }
    /**
     * Set one or more constraints to the query..
     * @param {Array<object>} array 
     */
    WHERE( ...conditions ){
        this.constraints = conditions.map( c => { 
            let parts = c.map( constraint => {
                if( typeof constraint == 'string' ){
                    return utils.operators.logicals[ constraint.toUpperCase() ] || 
                           utils.operators.logicals.or;
                }
                return utils.prepareConstraint( constraint );
            });
            return `(${ parts.join(' ') })`;
        }).join(' AND\n');
        return this;
    }
    ORDER_BY( column, dir){
        this.orderBy.push( `${
            utils.santizeLiterals( column.colName )}${ 
            /desc|asc/i.test( dir )? 
                ` ${ dir.toUpperCase()}` : 
                ''
        }`);
        return this;
    }
    LEFT_JOIN( joinedColumn, mainColumn ){
        this.joins.push(`LEFT JOIN ${
            joinedColumn.table
        } ON ${
            utils.formatTableWithColumn( mainColumn )
        } = ${
            utils.formatTableWithColumn( joinedColumn )
        }`);
        return this;
    }
    /**
     * Build query..
     * @returns {string} SQL query statment ready to be executed
     */
    EXEC(){
        //Abada Cadabra - Swish =`.+ Swosh
        this.query = `SELECT ${
            this.top? 
                `TOP(${ this.top }) ` : 
                ''
        }${ 
            this.cols 
        }\nFROM ${ 
            this.table 
        }${ 
            this.joins.length? 
                `\n${ this.joins.join('\n') }` : //Ironically ;) 
                ''
        }${
            this.constraints? `\nWHERE ${ this.constraints }` : ''
        }${ 
            this.orderBy.length? `\nORDER BY ${ this.orderBy.join(', ') }` : ''
        }`.trim();
        if( isDev && this.log ) 
            console.log( 'SQL query:\n', this.query );
        return this.query;
    }
}