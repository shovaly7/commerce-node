export const operators = {
    comparisons : {
        '>' : '>',
        '>=' : '>=',
        '<' : '<',
        '<=' : '<=',
        '=' : '=',
        'gt' : '>',
        'gteq' : '>=',
        'lt' : '<',
        'lteq' : '<=',
        'eq' : '='
    },
    logicals : {
        'and' : 'AND',
        '&&' : 'AND',
        'or' : 'OR',
        '||' : 'OR',
        'in' : 'IN'
    }
}

export const santizeLiterals = str => {
    if( !str ) return '';
    str = str.trim();
    str = str.replace(/\s/g, '_');
    return str.replace(/[\W]+/g,'');
}

export const prepareValue = (column, value) => {
    switch ( column.type ) {
        case 'string':
            return `'${ value.trim().replace(/'/g, "''") }'`;
        case 'integer':
            let parsedInt = parseInt( value );
            if(  isNaN( parsedInt ) )
                throw new Error(`Non-integer value for "${ column.name }"`)
            return parsedInt;
        case 'boolean':
            value = value == true || value == 1 ? 1 : 0;
            return value;
        default:
            throw new Error(`Missing column type for column "${ column.name }"`)
    }
}

export const setTableNameToColumn = (table, columns )=>{
    const iterateKeys = keyz =>{
        for( const i in keyz ){
            if( keyz.colName ){
                keyz.table = table;
            }
            else if( typeof keyz[i] == 'object' ){
                iterateKeys( keyz[i] );
            }
        }
    }
    iterateKeys( columns );
}
export const prepareColumnStructure = obj =>{
    const columns = [];
    const iterateKeys = keyz =>{
        for( const i in keyz ){
            if( keyz[i].colName ){
                columns.push( keyz[i] ); 
            }
            else if( typeof keyz[i] == 'object' ){
                iterateKeys( keyz[i] );
            }
        }
    }
    iterateKeys( obj );
    return columns;
}

export const prepareConstraint = constraint =>{
    const [col, op, value] = constraint;

    const operator = operators.comparisons[ op.toUpperCase() ];
    if( !operator ) throw new Error(`Invalid operator "${ op }"`);

    return `${col.colName} ${operator} ${ prepareValue( col, value ) }`;
}

export const formatTableWithColumn = columnObj => `${ columnObj.table }.${ columnObj.colName }`;