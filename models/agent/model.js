import { setTableNameToColumn } from '../utils.js'
export const table = 'Agents';
export const columns = {
	id: {
		colName: "ID",
		type: "integer"
	},
	bookingID: {
		colName: "BookingID",
		type: "integer"
	},	
	hsOwnerID: {
		colName: "OwnerID",
		type: "integer"
	},
	email: {
		colName: "Email",
		type: "string",
		alias : "AgentEmail"
	},
	name: {
		colName: "Name",
		type: "string",
		alias : "AgentName"
	},
	phone: {
		colName: "Phone",
		type: "string",
		alias : "AgentPhone"
	},
	picPath: {
		colName: "PicPath",
		type: "string"
	},
	isActive: {
		colName: "Active",
		type: "bit"
	},
	leadsFromCommerce: {
		colName: "LeadsFromCommerce",
		type: "bit"
	},
	leadsFromSite: {
		colName: "LeadsFromSite",
		type: "bit"
	},
	commerceLeadsCount: {
		colName: "LeadsCount",
		type: "integer"
	},
	siteLeadsCount: {
		colName: "SiteLeadsCount",
		type: "integer"
	},
	hsMeetingName: {
		colName: "hubSpotMeetingName",
		type: "string"
	}
}
setTableNameToColumn( table, columns );