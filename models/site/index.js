import validateNonEmpty, {dbRequest, dbErrorHandler } from '../../boot/db.js';
import { isValidSiteKey, isPositiveInteger } from '../../utils/santizer.js';

import moment from 'moment';
const approvedFields = {
    "name" : "Name",
    "phone" : "Phone",
}

const validateSiteKey = key =>{
    if( isValidSiteKey( key ) ) return true;
    throw new Error( `Malformed siteKey.` );
}

const validateUserID = id =>{
    if( isPositiveInteger( id ) ) return true;
    throw new Error( `Malformed User ID.` );
}

export const getDataBySiteKey = async (siteKey = validateNonEmpty('Site-Key')) => {
    validateSiteKey( siteKey );

    return dbRequest(`
        SELECT 
            ID,
            WWW as 'domain'
        FROM Accessibility_site
        Where Site_Key = '${siteKey}'`)
        .then( result =>{
            return result.recordset;
        })
        .catch( err => dbErrorHandler( err ));
}

export const getSiteStats = async (userID = validateNonEmpty('User ID')) =>{
    validateUserID( userID );

    return dbRequest(`
    SELECT 
        sum(CAST(soundreder as int)) as 'soundReader',
        sum(CAST(keyboard as int)) as 'keyboardNav',
        sum(CAST(smartNav as int)) as 'smartNav',
        sum(CAST(epilepsy as int)) as 'epilepsy',
        sum(CAST(textReader as int)) as 'textReader',
        sum(CAST(voiceComands as int)) as 'voiceCmd',
        sum(CAST(customcolor as int)) as 'customColor',
        sum(CAST(whiteblack as int)) as 'brightHC',
        sum(CAST(blackwhit as int)) as 'darkHC',
        sum(CAST(monochrome as int)) as 'monochrome',
        sum(CAST(fontsizeA as int)) as 'fontSize',
        sum(CAST(bigCursor as int)) as 'bigCursor',
        sum(CAST(magnifier as int)) as 'magnifier',
        sum(CAST(readableFont as int)) as 'readableFont',
        sum(CAST(imgDesc as int)) as 'imgDesc',
        sum(CAST(highlightLinks as int)) as 'hlLinks',
        sum(CAST(highlightHeaders as int)) as 'hlHeaders',
        sum(CAST(textMagnifier as int)) as 'textMagnifier',
        sum(CAST(dictionary as int)) as 'dictionary',
        sum(CAST(virtualKeyboard as int)) as 'virtualKeyboard'
    FROM Accessibility_stats
    LEFT JOIN Accessibility_site on Accessibility_stats.Site_KEY = Accessibility_site.Site_KEY
    Where Accessibility_site.USER_ID = '${ userID }'`)
    .then( result =>{
        return result.recordset;
    })
    .catch( err => dbErrorHandler( err ))
}