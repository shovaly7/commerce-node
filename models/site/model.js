import { setTableNameToColumn } from '../utils.js'
export const table = 'Accessibility_Site';
export const columns = {
	id: {
		colName: "ID",
		type: "integer"
	},
	createdOn: {
		colName: "dateSstart",
		type: "datetime",
		alias : "CreatedOn"
	},
	domain: {
		colName: "WWW",
		type: "string",
		alias : "Domain"
	},
	name: {
		colName: "Config6",
		type: "string",
		alias : "Name"
	},
	altDomains: {
		colName: "Config5",
		type: "string",
		alias : "AltDomains"
	},
	key: {
		colName: "Site_KEY",
		type: "string",
		alias : "SiteKey"
	},
	lang: {
		colName: "MenuLANG",
		type: "string",
		alias : "Lang"
	},
	platformID: {
		colName: "SiteTaye",
		type: "integer",
		alias : "PlatformID"
	},
	agentID: {
		colName: "Account_bookkeeping",
		type: "integer",
		alias : "AgentID"
	},
	ownerID: {
		colName: "USER_ID",
		type: "integer",
		alias : "OwnerID"
	},
	license: {
		isActive: {
			colName: "Disabled",
			type: "boolean",
			alias : "IsLicenseActive"
		},
		endDate: {
			colName: "dateEnd",
			type : "datetime",
			alias : "LicenseEndDate"
		},
		currentRetainer: {
			colName: "Payment_ListPrice",
			type : "decimal",
			alias : "CurrentRetainer"
		},
		retainerOffer: {
			colName: "LicensePriceOffer",
			type : "integer"
		},
		oneTimeCost: {
			colName: "OneTimeCost",
			type : "integer"
		},
	},
	payment: {
		type: {
			colName: "payment_type",
			type : "integer"
		},
		method: {
			colName: "Payment_Method",
			type : "integer"
		},
		currency: {
			colName: "Payment_Currency",
			type : "integer"
		},
		discount: {
			colName: "Payment_Discount",
			type : "integer"
		},
	},
	a11y: {
		version: {
			colName: "accVersion",
			type : "string"
		},
		mode: {
			colName: "DisabledOptions",
			type : "integer",
			alias : "A11yMode"
		},
		activations: {
			colName: "Contvisit",
			type : "integer",
			alias : "A11yActivations"
		},
		lastActivation: {
			colName: "lastdatevisit",
			type : "datetime",
			alias : "LastActivation"
		},
		statementLink: {
			colName: "Statement_link",
			type : "string"
		},
		planID: {
			colName: "LicenseType",
			type : "integer",
			alias : "PlanID"
		},
	},
	menuColors: {
		lead: {
			colName: "menucolor1",
			type : "string",
			alias : "LeadMenuColor"
		},
		active: {
			colName: "menucolor2",
			type : "string",
			alias : "ActiveMenuColor"
		},
		hover: {
			colName: "menucolor3",
			type : "string",
			alias : "HoverMenuColor"
		},
	}
}
setTableNameToColumn( table, columns );