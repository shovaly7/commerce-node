import {dbRequest, dbErrorHandler} from '../../boot/db.js';
import { columns as siteCols, table as siteTable } from '../site/model.js';
import { columns as monitorCols, table as monitorTable } from '../monitorPlan/model.js';
import { columns as agentCols, table as agentTable } from '../agent/model.js';
import { columns as userCols, table as userTable } from '../user/model.js';
import { columns as planCols, table as planTable } from '../plan/model.js';

import { SELECT } from '../query-builder.js';
import moment from 'moment';

/**
 * Get all site with agregators of "Limit" & "Offset"
 * @param {Number} limit 
 * @param {Number} offset 
 * @returns {promise<Array>}  Site from offset to limit..
 */
export const getSites = async ( limit, offset) => {
    if( isNaN( limit ) || isNaN( offset ) ) 
        return dbErrorHandler( 'Invalid agregator!');

    return dbRequest(`
        SELECT 
            ID,
            WWW as 'domain',
            SITE_Key as 'siteKey',
			USER_ID
        FROM Accessibility_site
        ORDER BY ID
        OFFSET ${offset} ROWS 
        FETCH NEXT ${limit} ROWS ONLY;`)
        .then( result =>{
            return result.recordset;
        })
        .catch( err => dbErrorHandler( err ));
        /*
            return dbRequest(`
                SELECT 
                    ID,
                    WWW as 'domain',
                    SITE_Key as 'siteKey'
                FROM Accessibility_site
                ORDER BY ID`)
        */
}
/**
 * Get all sites relevant details to save in Cache.
 * @returns {promise<Array>}  All sites in DB
 */
export const getAllSites = async () => {
	let monitorPlanName = planCols.name;
	monitorPlanName.alias = 'MonitorPlanName';//Overrides model alias to fit current request..
    return dbRequest(
		new SELECT({ log : false })
			.columns([
				siteCols.id,
				siteCols.lang,
				siteCols.domain,
				siteCols.key,
				userCols.email,
				userCols.name,
				monitorPlanName,
				agentCols.name
			])
			.FROM( siteTable )
			.LEFT_JOIN( monitorCols.siteID, siteCols.id )
			.LEFT_JOIN( agentCols.bookingID, siteCols.agentID )
			.LEFT_JOIN( userCols.id, siteCols.ownerID )
			.LEFT_JOIN( planCols.id, monitorCols.planID )
			.ORDER_BY( siteCols.id )
			.EXEC()
	)
        .then( result =>{
            return result.recordset;
        })
        .catch( err => dbErrorHandler( err ));
}
/*
{
	"site" : {
		"id" : <@column "A1.ID">,
		"createdOn" : "<@column "A1.dateSstart" format='datetime:%d/%m/%Y, %H:%M:%S'>",
		"domain" : "<@column "A1.WWW" encoding='javascript'>",
		"name" : "<@column "A1.Config6">",
		"altDomains" : "<@column "A1.Config5">",
		"key" : "<@column "A1.Site_KEY">",
		"lang" : "<@column "A1.MenuLANG">",
		"platform" : "<@column "PlatformID">"
	},
	"license" : {
		"isActive" : "<@column "A1.Disabled">",
		"endDate" : "<@column "A1.dateEnd">",
		"currentRetainer" : "<@column "A1.Payment_ListPrice">",
		"retainerOffer" : "<@column "A1.LicensePriceOffer">",
		"oneTimeCost" : "<@column "A1.OneTimeCost">"
	},
	"payment" : {
		"type" : "<@column "A1.payment_type">",
		"method" : "<@column "A1.Payment_Method">",
		"currency" : "<@column "A1.Payment_Currency">",
		"discount" : "<@column "A1.Payment_Discount">"
	},
	"a11y" : {
		"version" : "<@column "A1.accVersion">",
		"mode" : "<@column "A1.DisabledOptions">",		
		"activations" : "<@column "A1.Contvisit">",
		"lastActivation" : "<@column "A1.lastdatevisit">",
		"statementLink" : "<@column "A1.Statement_link">",
		"plan" : "<@column "PlanName">"
	},
	"monitor-plan" : {
		"id" : "<@column "MonitorPlanID">",
		"name" : "<@column "MonitorPlanName">",
		"maxPages" : "<@column "MonitorPages">"
	},
	"client" : {
		"id" : <@column "A1.USER_ID">,
		"name" : "<@column "ClientName">",
		"email" : "<@column "ClientEmail">",
		"createdOn" : "<@column "S1.Created" format='datetime:%d/%m/%Y, %H:%M:%S'>",
		"company" : "<@column "S1.Company">",
		"discount" : "<@column "ClientDiscount">"
	},
	"agent" : {
		"id" : "<@column "A1.Account_bookkeeping">",
		"name" : "<@column "AgentName">",
		"email" : "<@column "AgentEmail">"
	},
	"menu-colors" : {
		"lead" : "<@column "A1.menucolor1">",
		"active" : "<@column "A1.menucolor2">",
		"hover" : "<@column "A1.menucolor3">"
	}
}
*/