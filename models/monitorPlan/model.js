import { setTableNameToColumn } from '../utils.js'
export const table = 'MonitorPlans';
export const columns = {
	id: {
		colName: "ID",
		type: "integer"
	},
	createdOn: {
		colName: "createdOn",
		type: "datetime"
	},
	siteID : {
		colName: "siteID",
		type: "integer"
	},
	userID : {
		colName: "userID",
		type: "integer"
	},
	planID : {
		colName: "planID",
		type: "integer",
		alias : "MonitorPlanID"
	},
    payment: {
		type: {
			colName: "payType",
			type : "integer"
		},
		method: {
			colName: "payMethod",
			type : "integer"
		},
		currency: {
			colName: "payCurrency",
			type : "integer"
		}
	},
}
setTableNameToColumn( table, columns );