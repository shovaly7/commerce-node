import { createHash } from 'crypto';
export default (inputStr) => createHash('md5').update( inputStr ).digest('hex');