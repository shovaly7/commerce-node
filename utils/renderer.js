import * as flags from '../config/flags.js';
import {isLoggedIn} from './../utils/session.js';
import {
    isMonitorAdminGroupMember,
    isDevMember,
    isAgent,
    hasCurrencies
} from './user-role.js'

const tinyFooterID = {
    side : 'side',
    footer : 'bottom' 
}
const extractPageName = pageDir => pageDir.split('/').pop()

/**
 * Custom render function
 * @param {string} pageDir page directory relaltive to "views/" dir.
 * @param {Object} current {
 * req, res
 * } request and response instances 
 * @param {*} payload all other local vars..
 * @returns rendered HTML string
 */
export const render = ( pageDir, current, payload  = {}) =>{
    payload = {...flags, ...payload, tinyFooterID};
    payload.hasHeader = false;
    let bodyClasses = [];
    let isDevTeamMember = false;
    let loggedIn = isLoggedIn( current.req );
    let user = loggedIn && current.req.session.user || null;
    // if( !user ) current.req.session = {};
    if( flags.isCDK ) 
        bodyClasses.push( 'isCDK' );

    bodyClasses.push(`${flags.isDev? 'dev' : 'prod'}-environment`);
    bodyClasses.push(`is-${loggedIn? 'logged-in' : 'disconnected'}`);

    const authenticated = loggedIn? !user.shouldAuthenticate || user.doubleAuthenticated : false;
    if( authenticated ){
        payload.hasCurrencies = hasCurrencies( user );
        payload.user = user;
        payload.hasHeader = true;
        bodyClasses.push( 'authenticated-user has-header' );
        isDevTeamMember = isDevMember( user );
        payload.isAgent = isAgent( user );
        payload.userRole = null;
        if( isDevTeamMember ) 
            bodyClasses.push( 'is-dev-member' );

        switch( user.role ){
            case 'ADMIN':
            case 'PM':
            case 'AGENT':
                let _role = user.role.toLowerCase();
                payload.userRole = _role;
                bodyClasses.push( `${_role}-scope` );
        }
    }

    payload.page = extractPageName( pageDir );
    payload.pageDir = pageDir;
    payload.authenticated = authenticated;
    payload.isMonitorAdminGroupMember = authenticated && isMonitorAdminGroupMember( user );
    payload.isDevTeamMember = isDevTeamMember;
    payload.adminOverride = false;//Is it necessary?
    payload.bodyClasses = bodyClasses.join(' ');
    
    return current.res
            .type('html')
            .render( 
                current.req.query.async == 'true'? 
                    pageDir :
                    'master', 
                payload 
            );
}