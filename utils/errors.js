import {isDev} from '../config/flags.js';
const defaultErrors = {
    400 : 'Invalid parameters',
    401 : 'Unauthorized. Please login and try again.',
    402 : 'Payment required.',
    403 : 'Forbidden action. Please try a different one.',
    404 : 'Resource not found.',
    409 : 'This resource already exists. Please try to reset it or a different one.',
    423 : 'Resource Locked.',
    500 : 'An unexpected error occurred. Please try again later.'
}
/**
 * Main error handler
 * @param {object} res express res object
 * @param {string} message message to client
 * @param {Number} status numeric status code
 * @returns res object ready to client
 */
const errorWrapper = ( res, message, status ) =>{
    status = status || 500;
    let defMessage = defaultErrors[status];
    message = isDev? message || defMessage : defMessage;
    return res
        .status( status || 500 )
        .send({ error : message });
}

export const ResourceAlreadyExists = ( res, msg ) =>
    errorWrapper( res, msg, 409 );

export const ResourceLocked = ( res, msg ) =>
    errorWrapper( res, msg, 423 );

export const NotFound = (res, msg) => 
    errorWrapper( res, msg, 404 );

export const MissingOrInvalidParams = (res, msg ) => 
    errorWrapper( res, msg, 400 );

export const Unexpected = ( res, msg ) =>
    errorWrapper( res, msg, 500 );