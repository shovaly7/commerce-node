export const isAdmin = user => user?.role == 'ADMIN' || false;
export const isAgent = user => user?.role == 'AGENT' || false;
export const isPM = user => user?.role == 'PM' || false;

export const getRole = user =>{
    if( !user ) return null;
    return user.IsSystemAdmin? 
                'ADMIN' : 
                user.IsSalesManager? 
                    'AGENT' : 
                    user.IsProjectsManager? 
                        'PM' :
                        null;
}

export const isMonitorAdminGroupMember = user =>{
    if( !user ) return false;
    return isAdmin( user ) || 
           isAgent( user ) || 
           isPM( user );
}

export const isDevMember = user =>{
    if( !user ) return false;
    return user.email == 'shoval@nagich.co.il' ||
           user.email == 'shoval@equalweb.com' ||
           user.email == 'shoval-test@equalweb.com';
}

export const hasCurrencies = user =>{
    !isMonitorAdminGroupMember( user ) && 
    (!user.paymentType || user.paymentType == 1);
}