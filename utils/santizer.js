import PhoneNumber from 'awesome-phonenumber';
const passRE = new RegExp('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,50}');
const emailRE = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/**
 * Validate valid date ;-)
 * @param {string} date 
 * @returns {boolean}
 */
 export const isValidDate = date => {
 	return date && !isNaN( new Date( date ).getDate() ) || false;
 }

/**
 * Validate SiteKey format (Hexadecimal, 32 length)
 * @param {string} siteKey 
 * @returns {boolean}
 */
export const isValidSiteKey = siteKey => /^[a-fA-F0-9]{32}$/g.test( String( siteKey ) ); 

const maxLengthExceed = (str, maxLength) =>{ 
	return maxLength && str.length > maxLength || false;
}

/**
 * Check if password meets the format standard
 * @param {string} pass
 * @returns {boolean} 
 * Should have:
	- At least 8 characters.
    - At most 50 characters.
	- At least 1 digit.
	- At least 1 small letter.
	- At least 1 capital letter.
 */
export const isValidPassword = pass => passRE.test( pass );

/**
 * Check if e-mail is in appropriate format
 * @param {String} email
 * @param {Number} maxLength
 * @returns {boolean} 
 */
export const isValidEmail = ( email, maxLength ) => {
	email = String( email ).toLowerCase();
	if( maxLengthExceed( email, maxLength ) ) return false;
	return emailRE.test( String( email ).toLowerCase() );
}

/**
 * Validate a 6 length PIN code
 * @param {*} code 
 * @returns {boolean} 
 */
export const isValidPINcode = code => /^[\d+]{6}$/g.test( String( code ) );

/**
 * Validate a positive number without integer safety check
 * @param {number} input 
 * @returns {boolean} 
 */
export const isPositiveInteger = ( input ) => /^[\d]+$/g.test( String( input ) ) && input > 0;

/**
 * Validate an alphanumeric input (Numbers & english letters only).
 * @param {string} input 
 * @returns {boolean} 
 */
export const isAlphaNumeric = ( input ) => input && /^[a-zA-Z0-9]+$/g.test( String( input ) ) || false;

/**
 * Validate an appropriate HTTP/s URL string.
 * @param {string} url 
 * @returns {boolean} 
 */
 export const isValidURL = ( url ) => {
	 try{
		let urlObj = new URL( url );
		return urlObj.protocol == 'https:' || urlObj.protocol == 'http:';
	 } catch( err ) {
		 return false;
	 }
 }

/**
 * @doc based on https://www.npmjs.com/package/awesome-phonenumber
 * @param {string} phoneNumber raw phone number, with or without prefix..
 * @param {string|null} countryCode not nessesary if number is "E.164" formatted..
 * @returns {string} E.164 formatted phone number.
 */
export const formatPhoneNumber = (phoneNumber, countryCode) =>{
	if( !phoneNumber ) return null;

	let pn = new PhoneNumber( phoneNumber );
	if( pn.isValid() ) return pn.getNumber();
	else {
		pn = new PhoneNumber( phoneNumber, countryCode );
		return pn.isValid()? 
			pn.getNumber() : 
			null;	
	}
}

/**
 * Remove all logical chars..
 * @param {string} name 
 */
export const formatName = (name, maxLength) =>{
	let sanitized = name.replace(/<|>|\*|`|'|"|,|\\|\/|=|\(|\)|"|\|/g,'');
    sanitized = sanitized.trim().replace(/\s+/g, ' ');
	if( maxLengthExceed( name, maxLength ) ) return false;
	return sanitized;
}