import * as errors from '../utils/errors.js';
import {logIn} from '../models/user/index.js';
import {getRole} from '../utils/user-role.js';
import {maskEmail, maskPhoneNumber} from '../utils/helpers.js';

const doesPasswordExpired = expiresOn => expiresOn < Date.now();

export const isLoggedIn = req => req?.session?.user?.loggedIn || false;

export const logOut = (req, res, next) => {
    // req.session?.destroy();
    // next();
    res.clearCookie('sid');
    req.session.user = null;
    req.session.regenerate( err => {
      if( err ) 
        return errors.Unexpected(res, 'Error while renewing session..');
      next();
    });
}

export const flashTempUser = (req, res, next) =>{
    if( req.session && req.session.tempUser ){
        delete req.session.tempUser;
    }
    next();
}

export const tryToLogin = (req, res, next) =>{
    logIn( res.locals.email )
      .then( user =>{
          if( !user ){
            //No such user, in DB..
            return NotFoundError( res );
          }
          res.locals.user = user; 
          next();
      })
      .catch( err =>{
        console.error( err );
        return errors.Unexpected( res );
      })
}

export const saveTempUserSession = (req, res, next) =>{
    let user = res.locals.user;
    if( !user ) errors.NotFound( res, 'User not found.' );

    req.session.regenerate( err => {
      if( err ) 
        return errors.Unexpected( res );
  
      req.session.tempUser = {
          name: user.name,
          email: user.email,
          maskedEmail: maskEmail( user.email ),
          phone : user.phone,
          maskedPhone: maskPhoneNumber( user.phone ),
          password : user.password,
          country : user.userCountry
      }
      next();
    });
}

export const saveUserSession = (req, res, next) =>{
    let user = res.locals.user;
    if( !user ) errors.NotFound( res, 'User not found.' );

    req.session.regenerate( err => {
      if( err ) 
        return errors.Unexpected( res );
  
      req.session.user = {
          loggedIn : true,
          id: user.userID.toString(),
          name: user.userName,
          email: user.userEmail,
          phone : user.userPhone,
          countryCode : user.userCountry,
          passExpired : doesPasswordExpired( user.passExpiration ),
          activated : user.activated == 'activated',
          shouldAuthenticate : user.shouldAuthenticate || false,
          paymentCurrency : user.paymentCurrency || 'USD',
          paymentType : user.paymentType,
          role : getRole( user )
      }
      next();
    });
}