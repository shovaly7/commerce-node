import md5Hash from '../utils/md5-hash.js';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

//export const doesDatePassed = date => date > Date.now();

export const validateNonEmpty = fieldName =>{
    throw new Error( `Missing ${fieldName} field.` );
}
export const setLoginHash = req => {
    let agentAndIP = `${
        req.headers["user-agent"] }${
        req.headers['x-forwarded-for'] || req.ip
    }`;
    return md5Hash( agentAndIP );
}

export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

export const getDirname = fileURL => dirname( fileURLToPath( fileURL ) );

export const maskPhoneNumber = (phoneNumber = '') =>{
    phoneNumber = phoneNumber.slice(-4);
    return phoneNumber.padStart(8, '*');
}

export const maskEmail = (email = '') => {
    let [name, domain] = email.split('@');
    const { length: len } = name;
    let quarter = len / 4;
    return `${name.substr(0, len / quarter)}....${name.slice(-quarter)}@${domain}`;
};